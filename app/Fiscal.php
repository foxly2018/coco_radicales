<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fiscal extends Model
{
    protected $table = 'fiscales';
    protected $primaryKey = 'idfiscales';

    protected $fillable = [
        'id_voluntario','circuito_fiscal','id_mesa','id_escuela','tipo','asistencia','capacitacion'
    ];

    
     //1:1
     public function voluntario()
     {
         return $this->hasOne('App\Voluntario','idvoluntario','id_voluntario');
     }

     public function circuitofiscal()
     {
         return $this->hasOne('App\CircuitoFiscal','idcircuitofiscal','circuito_fiscal');
     }

     public function escuela()
     {
         return $this->hasOne('App\Escuela','idescuela','id_escuela');
     }
}

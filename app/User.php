<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre','apellido','dni','celular','telefono','fecha_nacimiento','email', 'password','estado',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    //1:1
    public function direccion()
    {
        return $this->hasOne('App\Direccion','iddireccion','direccion_iddireccion');
    }
    /* //1:1
    public function circuito()
    {
        return $this->hasOne('App\Circuito','idcircuito','circuito_idcircuito');
    } */
    //N:M
    public function documentos()
    {
        return $this->belongsToMany('App\Documento', 'doc_users', 'users_idusuario', 'documentos_iddocumento');
    }
    //N:M
    public function circuitos()
    {
        return $this->belongsToMany('App\Circuito', 'circuitos_users', 'users_id', 'circuitos_idcircuito')
                    ->withPivot('id','es_responsable');
    }
    //N:M
    public function localidades()
    {
        return $this->belongsToMany('App\Localidad', 'localidades_users', 'users_id', 'localidades_idlocalidad')
                    ->withPivot('id','es_responsable');
    }
    //N:M
    public function interacciones()
    {
        return $this->belongsToMany('App\Voluntario', 'interacciones', 'users_id', 'voluntarios_idvoluntario');
    }
}

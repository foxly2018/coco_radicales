<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CircuitoFiscal extends Model
{
    //
    protected $table = 'circuito_fiscales';
    protected $primaryKey = 'idcircuitofiscal';

    protected $fillable = [
        'circuito_nombre',
    ];
    //1:N invrsa
    //cada localidad tiene muchos circuitos
    public function localidad()
    {
        return $this->belongsTo('App\Localidad','localidad_idlocalidad','idlocalidad');
    }
     //1:1 inversa
     //1:M
     //cada circuito puede tener muchas escuelas
     public function escuela()
     {
         return $this->hasMany('App\Escuela','circuitofiscal','idcircuitofiscal');
     }
    
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mesa extends Model
{
    protected $table = 'mesas';
    protected $primaryKey = 'idmesa';

    protected $fillable = [
        'numero_mesa','disponible','estado','id_escuela',
    ];

    
     //1:1
     public function escuela()
     {
         return $this->belongsTo('App\Escuela','idescuela','id_escuela');
     }
}

<?php

namespace App\Http\Controllers;

use App\Circuito;
use App\Departamento;
use App\Direccion;
//use App\Documento;
use App\Localidad;

use App\Mesa;
use App\Escuela;
use App\CircuitoFiscal;
use App\Voluntario;
use App\Fiscal;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB; 

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Hash;



class FiscalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $escuelas= DB::table('escuelas')
        ->join('circuito_fiscales','escuelas.circuitofiscal','circuito_fiscales.idcircuitofiscal')
        ->join('users','escuelas.fiscal_general','users.id')
                        ->select('escuelas.*', 'circuito_fiscales.circuito_nombre as circuito', 'users.nombre as nombre_fiscal', 'users.apellido as apellido_fiscal', 'users.celular as celular')
                        ->where('escuelas.disponible','si')
                        ->get();
       
        return view('fiscales.index',compact('escuelas')); 

           /*  $fiscales = DB::table('fiscales')
            ->join('voluntarios','fiscales.id_voluntario','voluntarios.idvoluntario')
            ->join('circuito_fiscales','fiscales.circuito_fiscal','circuito_fiscales.idcircuitofiscal') 
            ->join('mesas','fiscales.id_mesa','mesas.idmesa')
            ->join('escuelas','fiscales.id_escuela','escuelas.idescuela')
            ->select('fiscales.*','voluntarios.nombre','voluntarios.apellido','voluntarios.celular','circuito_fiscales.circuito_nombre as circuito', 'mesas.numero_mesa as mesa', 'escuelas.nombre_escuela as escuela')
            ->get(); */
      
      
            /* $voluntarios = DB::table('voluntarios')
           ->join('circuitos','voluntarios.circuito_idcircuito','circuitos.idcircuito')
           ->join('localidades','circuitos.localidad_idlocalidad','localidades.idlocalidad')
           ->select('voluntarios.*')
           ->where('voluntarios.estado',1)
           ->where('voluntarios.serFiscal',1)
           ->orderBy('voluntarios.circuito_idcircuito', 'asc')
           ->get(); */
        
          // return view('fiscales.index',compact('fiscales'));
           
           
           //return "esto es fiscales"; 
   
        }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
       
        //$escuelas = Escuela::all();
        $escuelas = DB::table('escuelas')
        ->join('circuito_fiscales', 'escuelas.circuitofiscal', 'circuito_fiscales.idcircuitofiscal')
        ->select('escuelas.*', 'circuito_fiscales.circuito_nombre as circuito_escuela')
        ->where('escuelas.disponible','si')
        ->where('escuelas.estado','incompleto')
        ->wherenull('escuelas.designada')
        ->get();

       // $mesas = Mesa::all()->where('disponible','si');
        $dirigentes = DB::table('users')
        ->select('users.*')
        ->wherenull('users.serFiscal') 
        ->get();

        return view('fiscales.create', compact('escuelas','dirigentes')); 
       // return "esto es fiscales"; 
    }

    public function store(Request $request)
    {
        $comprobarIdDirigente = $request-> dirigente;
        $escuelas = DB::table('escuelas')->select('fiscal_general')->where('fiscal_general','=',$comprobarIdDirigente)->get();
        
        if(count($escuelas)>=1){
            return back()->withInput($request->all())
            ->with('warning','El dirigente ya es fiscal general');
        }
        else{
            $d = $request->dirigente;
            $e = $request->escuela;
           
            DB::table('escuelas')
            ->where('idescuela', $e)
            ->update(['fiscal_general' => $d , 'designada' => "si"]);

            DB::table('users')
            ->where('id', $request->dirigente)
            ->update(['serFiscal' => 2 ]); // 2 significa ya asignado

           

            return redirect()->route('fiscales.index')->with('success','Fiscal General asignado correctamente');
        }

    }

   // public function destroy(Fiscal $fiscal)
   public function destroy(Fiscal $fiscal)
    {
        $d = Fiscal::find($id);
        $voluntario = $d->id_voluntario;
        $escuela= $d->id_escuela;
        $mesa = $d->id_mesa;

        DB::table('voluntarios')
        ->where('idvoluntario', $voluntario)
        ->update(['serFiscal' => "" ]);

        DB::table('escuelas')
        ->where('idescuela', $escuela)
        ->update(['estado' =>"incompleto"]);

        DB::table('escuelas')->where('escuelas.idescuela','=', $escuela)->increment('faltan_mesas');

        DB::table('mesas')
        ->where('idmesa', $mesa)
        ->update(['disponible' => "si"]);
        
        $d->delete();
        
        return redirect()->route('fiscales.index')->with('warning','Fiscal Eliminado correctamente');

        
        
    }

    public function show(Request $request,$id)
    {
        $escuela = Escuela::find($id);
        $fiscales = DB::table('fiscales')
        ->join('voluntarios', 'fiscales.id_voluntario', 'voluntarios.idvoluntario')
        ->join('mesas', 'fiscales.id_mesa', 'mesas.idmesa')
        ->select('fiscales.*', 'voluntarios.nombre as nombre', 'voluntarios.apellido as apellido', 'voluntarios.celular as celular', 'mesas.numero_mesa as mesa')
        ->where('mesas.id_escuela', '=', $escuela->idescuela)
        ->get();
       
        $mesas = DB::table('mesas')
        ->join('escuelas', 'mesas.id_escuela', 'escuelas.idescuela')
        ->select('mesas.*', 'escuelas.nombre_escuela as escuela_mesa')
        ->where('mesas.id_escuela', '=', $escuela->idescuela)
        ->get(); 
        //$mesas = Mesa::find($escuela->idescuela);
        //$fiscalmesa = Voluntario::find($mesas->idmesa);

        return view('fiscales.escuela', compact('escuela', 'mesas','fiscales'));
        //return view('fiscales.index', compact('escuelas')); 

        
    }

    
    public function createFiscal(Request $request, $id)
    {
        $escuelaactual = Escuela::find($id);
       

       $voluntarios = DB::table('voluntarios')
        ->join('circuitos','voluntarios.circuito_idcircuito','circuitos.idcircuito')
        ->join('localidades','circuitos.localidad_idlocalidad','localidades.idlocalidad')
        ->select('voluntarios.*', 'circuitos.circuito_nombre as cto_nombre')
        ->where('voluntarios.estado',1)
        ->wherenull('voluntarios.serFiscal') 
        ->orderBy('voluntarios.circuito_idcircuito', 'asc')
        ->get();

        $mesas = DB::table('mesas')
        ->join('escuelas', 'mesas.id_escuela', 'escuelas.idescuela')
        ->select('mesas.*', 'escuelas.nombre_escuela as escuela_mesa')
        ->where('mesas.id_escuela','=',$escuelaactual->idescuela)
        ->where('mesas.disponible',"si")
        ->get(); 

       return view('fiscales.createMesa', compact('voluntarios','mesas','escuelaactual')); 
       // return "esto es fiscales mesa"; 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeMesa(Request $request)
    {
       $comprobarIdMesa = $request-> mesas;
        $fiscales = DB::table('fiscales')->select('id_mesa')->where('id_mesa','=', $comprobarIdMesa)->get();
        if(count($fiscales) >=1) {
            return back()->withInput($request->all())
            ->with('warning','El fiscal ya tiene mesa');
        }
        else{
            $f=new Fiscal();
            $f->id_voluntario = $request->voluntario;
            $f->id_mesa = $request->mesas;
            $f->id_escuela = $request->escuela;
            $f->tipo = 'mesa';
            
            $f->save();
            
            DB::table('mesas')
            ->where('idmesa', $f->id_mesa)
            ->update(['disponible' => "no"]);

            DB::table('voluntarios')
            ->where('idvoluntario', $f->id_voluntario)
            ->update(['serFiscal' => 2 ]); // 2 significa ya asignado

            DB::table('voluntarios')
            ->where('idvoluntario', $f->id_voluntario)
            ->update(['serFiscal' => 2 ]); // 2 significa ya asignado


            $faltante = DB::table('escuelas')->select('faltan_mesas')->where('idescuela',  $f->id_escuela)->get();
            $falta = $faltante[0]->faltan_mesas;
            //$falta2= $faltante(0)->faltan_mesas;
           // return ($faltante.'-'.$falta);
            //dd($falta); 

            if($falta >=2 ) {
                DB::table('escuelas')->where('escuelas.idescuela','=', $request->escuela)->decrement('faltan_mesas');
               //resta una mesa para mostrar las faltantes
            }
            else{
                DB::table('escuelas')->where('escuelas.idescuela','=', $request->escuela)->decrement('faltan_mesas');
               
                DB::table('escuelas')
                ->where('escuelas.idescuela','=', $request->escuela)
                ->update(['estado'=> 'completo']);
                //resta la ultima mesa y cambia el estado de la escuela a completo       
            }
        
            
            return redirect()->route('fiscales.index')->with('success','Fiscal Cargado Correctamente');
        }
    }


    public function showFiscal(Fiscal $fiscal)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Fiscal  $fiscal
     * @return \Illuminate\Http\Response
     */
   

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Fiscal  $fiscal
     * @return \Illuminate\Http\Response
     */
    public function edit(Fiscal $fiscal)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Fiscal  $fiscal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Fiscal $fiscal)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Fiscal  $fiscal
     * @return \Illuminate\Http\Response
     */
   
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\DB;
use App\Circuito;
use App\Departamento;
use App\Direccion;
use App\Documento;
use App\Localidad;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Hash;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class DirigentesController extends Controller
{
    public function index()
    {
        if (auth()->user()->hasRole('Administrador')) {
            $dirigentes = User::All()->where('estado',1);
           $array_dirigentes = [];
            foreach ($dirigentes as $u) {
                if ($u->hasRole('Dirigente')) {
                    array_push($array_dirigentes,$u);
                }
            }
            //$cPropio = User::find($array_dirigentes[0]->id)->circuitos[0]->idcircuito;
            //dd($cPropio);
        } else {
            if (auth()->user()->direccion_iddireccion != null) {
                $iddpto = auth()->user()->direccion->localidad->departamento->iddepartamento;
            } else {
                $iddpto = 0;
            }
            $array_dirigentes = DB::table('users')
            ->join('circuitos_users','circuitos_users.users_id','users.id')
            ->join('circuitos','circuitos_idcircuito','circuitos.idcircuito')
            ->join('direcciones','users.direccion_iddireccion','direcciones.iddireccion')
            ->join('model_has_roles','model_id','users.id')
            ->join('roles','role_id','roles.id')
            ->select('users.*')
            ->where('users.estado',1)
            ->where('roles.id',3)
            ->where('circuitos_users.es_responsable',2)
            ->where(function($query)
            {
                $b = false;
                foreach (auth()->user()->circuitos()->wherePivot('es_responsable',1)->get() as $c) {
                    $b = true;
                    $query->orWhere('circuitos.idcircuito','=', $c->idcircuito);
                }
                foreach (auth()->user()->localidades()->wherePivot('es_responsable',1)->get() as $l) {
                    $b = true;
                    $query->orWhere('direcciones.localidad_idlocalidad','=', $l->idlocalidad);
                }
                if (!$b) {
                    $query->Where('circuitos.circuito_nombre','=', '');
                }
            })
            ->get();
        }
          
        return view('dirigentes.index',compact('array_dirigentes'));
    }
    public function create()
    {
        $departamentos = Departamento::all();
        $localidades = Localidad::all();
        $circuitos = Circuito::all();
        $roles = Role::All();
        $permisos = Permission::all();
        //dd($u = auth()->user()->getRoleNames()[0]);
        return view('dirigentes.create', compact('departamentos','localidades','circuitos','roles','permisos'));
    }

    public function store(Request $request)
    {
        $validado = $this->validarRequest($request);
            if ($validado->fails()) {
                return back()
                            ->withErrors($validado)
                            ->withInput($request->all());
            }
        $request->validate([
            'file' => 'image|mimes:jpeg,png,jpg,gif',
            'password'=>'required',
        ],[
            'file' => 'Debe seleccionar un archivo de tipo jpeg,png,jpg,gif',
            'password'=>'La Contraseña es requerida',
        ]);
        $d = new Direccion();
        $d->calle = $request->calle;
        $d->numero = $request->numero;
        $d->piso = $request->piso;
        $d->dpto = $request->dpto;
        $d->manzana = $request->manzana;
        $d->casa = $request->casa;  
        $d->localidad_idlocalidad = $request->localidad;
        $d->save();

        $dirigente = new User();
        $dirigente->nombre = $request->nombre;
        $dirigente->apellido = $request->apellido;
        $dirigente->dni = $request->dni;
        $dirigente->celular = $request->celular;
        $dirigente->telefono = $request->telefono;
        $dirigente->fecha_nacimiento = $request->fecha_nacimiento;
        $dirigente->email = $request->email;
        $dirigente->direccion_iddireccion = $d->iddireccion;
        $dirigente->estado = 1;
        $dirigente->password = Hash::make($request->password);

        if ($request->file('file') != null) {
            if ( !Storage::disk('public')->exists("archivos_doc/imagenes/perfil")) {
                if(Storage::disk('public')->makeDirectory("/archivos_doc/imagenes/perfil" )){
                    //dd("creo la ruta");
                    $ruta = public_path()."/archivos_doc/imagenes/perfil";
                }else{                
                    //dd("No creo nada devolver algo un error ");
                    return redirect()->route('usuarios.index')->with('warning','Usuario Creado, "Foto no creada"');
                }            
            }else{
                //dd("entro al else ya existe la carpeta");
                $ruta = public_path()."/archivos_doc/imagenes/perfil";
            }
            $file = $request->file('file');
    
            $fileName = uniqid() . $file->getClientOriginalName();
        
            $file->move($ruta, $fileName);

            $dirigente->foto = "/archivos_doc/imagenes/perfil/".$fileName;
        }
        $dirigente->save();
        $dirigente->assignRole($request->roles);
        $dirigente->circuitos()->attach($request->circuitos, ['es_responsable' => 2]);//con 2 indico que este circuito es propio de el


        $resp_circuitos = json_decode( $request->resp_circuitos );
        if(  count($resp_circuitos) > 0){
            foreach ($resp_circuitos as $key) {
                $dirigente->circuitos()->attach( $key->id , ['es_responsable' => 1]);
            }
        }
        $resp_localidades = json_decode( $request->resp_localidades );
        if(  count($resp_localidades) > 0){
            foreach ($resp_localidades as $key) {
                $dirigente->localidades()->attach( $key->id , ['es_responsable' => 1]);
            }
        }
        
        return redirect()->route('dirigentes.index')->with('success','Dirigente Cargado Correctamente');
    }

    public function show(Request $request,$id)
    {
        $dirigente = User::find($id);
        if ($dirigente->direccion_iddireccion != null) {
            $dir = Direccion::find($dirigente->direccion_iddireccion);
            $localidad = Localidad::find($dir->localidad_idlocalidad); 
            $dpto = Departamento::find($localidad->departamento_iddepartamento);
        } else {
            $dir = "sin direccion";
            $localidad = "sin localidad"; 
            $dpto = "sin departamento";
        }
        $resp_circ = $dirigente->circuitos()->wherePivot('es_responsable',1)->get();
        $resp_loc = $dirigente->localidades()->wherePivot('es_responsable',1)->get();
        
        if ($request->ajax()) {
            return response()->json([
                'usuario' => $dirigente,
                'direccion' => $dir,
                'localidad' => $localidad,
                'departamento' => $dpto,
                'respCircuitos' => $resp_circ,
                'respLocalidades' => $resp_loc,
                'status' => true
            ]);
        }else{
            return response()->json([
                'status' => false
            ]);
        }
    }

    public function edit($id)
    {
        $departamentos = Departamento::all();
        $localidades = Localidad::all();
        $circuitos = Circuito::all();
        $dirigente = User::find($id);
        $roles = Role::All();
        $dir = Direccion::find($dirigente->direccion_iddireccion);

        if ($dir!= null) {
            $local = Localidad::find($dir->localidad_idlocalidad); 
            $dpto = Departamento::find($local->departamento_iddepartamento);
        } else {
            $local =null;
            $dpto =null;
        }
        
        return view('dirigentes.edit', compact('departamentos','localidades','circuitos','dirigente','dir','local','dpto','roles'));
    }

    public function update(Request $request, $id)
    {
        $validado = $this->validarRequest($request);
            if ($validado->fails()) {
                return back()
                            ->withErrors($validado)
                            ->withInput($request->all());
            } 
            $request->validate([
                'file' => 'image|mimes:jpeg,png,jpg,gif',
            ],[
                'file' => 'Debe seleccionar un archivo de tipo jpeg,png,jpg,gif',
            ]);
        $dirigente = User::find($id);
        if ($dirigente->direccion_iddireccion != null) {
            $d = Direccion::find($dirigente->direccion_iddireccion);
            $d->calle = $request->calle;
            $d->numero = $request->numero;
            $d->piso = $request->piso;
            $d->dpto = $request->dpto;
            $d->manzana = $request->manzana;
            $d->casa = $request->casa;  
            $d->localidad_idlocalidad = $request->localidad;
            $d->save();
        } else {
            $d = new Direccion();
            $d->calle = $request->calle;
            $d->numero = $request->numero;
            $d->piso = $request->piso;
            $d->dpto = $request->dpto;
            $d->manzana = $request->manzana;
            $d->casa = $request->casa;  
            $d->localidad_idlocalidad = $request->localidad;
            $d->save();
        }      

        $dirigente->nombre = $request->nombre;
        $dirigente->apellido = $request->apellido;
        $dirigente->dni = $request->dni;
        $dirigente->celular = $request->celular;
        $dirigente->telefono = $request->telefono;
        $dirigente->fecha_nacimiento = $request->fecha_nacimiento;
        $dirigente->email = $request->email;
        $dirigente->direccion_iddireccion = $d->iddireccion;
        $dirigente->estado = 1;
        if ($request->password != null) {
            $dirigente->password = Hash::make($request->password);
        }

        if ($request->file('file') != null) {
            if ( !Storage::disk('public')->exists("archivos_doc/imagenes/perfil")) {
                if(Storage::disk('public')->makeDirectory("/archivos_doc/imagenes/perfil" )){
                    //dd("creo la ruta");
                    $ruta = public_path()."/archivos_doc/imagenes/perfil";
                }else{                
                    //dd("No creo nada devolver algo un error ");
                    return redirect()->route('home')->with('warning','Dirigente Editado, "Foto no creada"');
                }            
            }else{
                //dd("entro al else ya existe la carpeta");
                $ruta = public_path()."/archivos_doc/imagenes/perfil";
            }
            if($dirigente->foto != null){
                $mi_imagen = public_path().$dirigente->foto;
                if (@getimagesize($mi_imagen)) {
                // echo "El archivo existe";
                    unlink($mi_imagen);
                }
            }
            $file = $request->file('file');
    
            $fileName = uniqid() . $file->getClientOriginalName();
        
            $file->move($ruta, $fileName);

            $dirigente->foto = "/archivos_doc/imagenes/perfil/".$fileName;
        }
        $dirigente->save();
        $dirigente->syncRoles($request->roles);

        $circuitoPropio = $dirigente->circuitos()->wherePivot('es_responsable',2)->get()->toArray();
        if($request->circuitos != $circuitoPropio[0]['idcircuito']){

            $dirigente->circuitos()->detach($circuitoPropio[0]['idcircuito']);
            $dirigente->circuitos()->attach([
                                $request->circuitos => ['es_responsable'=> 2]
                            ]);
        }

        // son los tags de responsabilidades
        $resp_circuitos =  json_decode( $request->resp_circuitos );      

        $circuitosActuales = $dirigente->circuitos()->wherePivot('es_responsable',1)->get()->toArray();
        $clave = 'idcircuito';
        $sync_circuitos = $this->conjuntoExcluyente($circuitosActuales,$resp_circuitos,$clave);
        // pos [0] = array de id actuales
        // pos [1] = array de id excluyentes
        foreach ($sync_circuitos[1] as $idCircuito) {
            if(in_array($idCircuito,$sync_circuitos[0])){
                $dirigente->circuitos()->detach($idCircuito);
            }else{
                $dirigente->circuitos()->attach([
                    $idCircuito => ['es_responsable'=> 1]
                ]);
            }
        }

        $resp_localidades = json_decode( $request->resp_localidades );
        $clave = 'idlocalidad';
        $localidadesActuales = $dirigente->localidades()->wherePivot('es_responsable',1)->get()->toArray();
        $sync_localidades = $this->conjuntoExcluyente($localidadesActuales,$resp_localidades,$clave);
        // pos [0] = array de id actuales
        // pos [1] = array de id excluyentes
        foreach ($sync_localidades[1] as $idLocadidad) {
            if(in_array($idLocadidad,$sync_localidades[0])){
                $dirigente->localidades()->detach($idLocadidad);
            }else{
                $dirigente->localidades()->attach([
                    $idLocadidad => ['es_responsable'=> 1]
                ]);
            }
        }
        //$dirigente->circuitos()->sync($request->circuitos);

        return redirect()->route('dirigentes.index')->with('success','Dirigente Editado Correctamente');
    }

    public function destroy($id)
    {
        $dirigente = User::find($id);
        $dirigente->estado = 2;
        $dirigente->save();
        return redirect()->route('dirigentes.index')->with('warning','Dirigente Eliminado correctamente');
    }
    public function validarRequest(Request $request)
    {
        $validado = Validator::make($request->all(), [
            'calle' => 'required',
            'numero'=>'required|numeric',
            'localidad'=>'required',
            'circuitos'=>'required',
            'departamento'=>'required',
            'nombre' => 'required',
            'apellido' => 'required',
            'dni'=>'required|numeric',
            'celular'=>'required|numeric',
            'fecha_nacimiento' => 'required',
            'email' => 'required',
            'roles'=>'required',
            //'file' => 'mimes:jpeg,png,jpg,gif',
        ],[
            'calle' => 'La calle es requerida',
            'numero'=>'El numero es requerido',
            'localidad'=>'La localidad es requerido',
            'circuitos'=>'El circuito es requerido',
            'departamento'=>'El departamento es requerido',
            'nombre' => 'El nombre es requerido',
            'apellido' => 'El apellido es requerido',
            'dni'=>'El DNI es requerido',
            'celular'=>'El celular es requerido',
            'fecha_nacimiento' => 'La fecha es requerida',
            'email' => 'El Correo es requerido',
            'roles'=>'El rol es requerido',
            //'file' => 'el archivo debe ser de tipo jpeg,png o jpg',
        ]);
        return $validado;

    }

    public function conjuntoExcluyente($elementosActuales,$elementosEdit,$clave)
    {
        $arrays = [];
        $ids_actuales = array_map(function ($elem) use ($clave)
        {
           return $elem[$clave];
        },$elementosActuales);

        $ids_edit = array_map(function ($elem)
        {
           return intval($elem->id);
        },$elementosEdit);

        $interseccion = array_uintersect($ids_actuales,$ids_edit,"strcasecmp");
       // $ids_sin_repetir = array_unique(array_merge($ids_actuales,$ids_edit));
        
        $union_ids = array_unique(array_merge($ids_actuales,$ids_edit));
        $ids_excluyentes = array_filter($union_ids,function ($elem) use ($interseccion)
        {
            return !in_array($elem, $interseccion);
        });
       // dd($union_ids);
        array_push($arrays,$ids_actuales);
        array_push($arrays,$ids_excluyentes);
        return $arrays;
        
    }
}

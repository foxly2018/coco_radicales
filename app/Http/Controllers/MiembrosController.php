<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Circuito;
use App\Departamento;
use App\Direccion;
use App\Documento;
use App\Localidad;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Hash;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class MiembrosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $miembros = User::All()->where('estado',1);
        $array_miembros = [];
        foreach ($miembros as $u) {
            if ($u->hasRole('Miembro')) {
                array_push($array_miembros,$u);
            }
        }
        return view('miembros.index',compact('array_miembros'));
    }

    public function create()
    {
        $departamentos = Departamento::all();
        $localidades = Localidad::all();
        $circuitos = Circuito::all();
        $roles = Role::All();
        $permisos = Permission::all();
        //dd($u = auth()->user()->getRoleNames()[0]);
        return view('miembros.create', compact('departamentos','localidades','circuitos','roles','permisos'));
    }

    public function store(Request $request)
    {
        $validado = $this->validarRequest($request);
            if ($validado->fails()) {
                return back()
                            ->withErrors($validado)
                            ->withInput($request->all());
            }
        $request->validate([
            'file' => 'image|mimes:jpeg,png,jpg,gif',
            'password'=>'required',
        ],[
            'file' => 'Debe seleccionar un archivo de tipo jpeg,png,jpg,gif',
            'password'=>'La Contraseña es requerida',
        ]);
        $d = new Direccion();
        $d->calle = $request->calle;
        $d->numero = $request->numero;
        $d->piso = $request->piso;
        $d->dpto = $request->dpto;
        $d->manzana = $request->manzana;
        $d->casa = $request->casa;  
        $d->localidad_idlocalidad = $request->localidad;
        $d->save();

        $coord = new User();
        $coord->nombre = $request->nombre;
        $coord->apellido = $request->apellido;
        $coord->dni = $request->dni;
        $coord->celular = $request->celular;
        $coord->telefono = $request->telefono;
        $coord->fecha_nacimiento = $request->fecha_nacimiento;
        $coord->email = $request->email;
        $coord->direccion_iddireccion = $d->iddireccion;
        $coord->estado = 1;
        $coord->password = Hash::make($request->password);

        if ($request->file('file') != null) {
            if ( !Storage::disk('public')->exists("archivos_doc/imagenes/perfil")) {
                if(Storage::disk('public')->makeDirectory("/archivos_doc/imagenes/perfil" )){
                    //dd("creo la ruta");
                    $ruta = public_path()."/archivos_doc/imagenes/perfil";
                }else{                
                    //dd("No creo nada devolver algo un error ");
                    return redirect()->route('usuarios.index')->with('warning','Usuario Creado, "Foto no creada"');
                }            
            }else{
                //dd("entro al else ya existe la carpeta");
                $ruta = public_path()."/archivos_doc/imagenes/perfil";
            }
            $file = $request->file('file');
    
            $fileName = uniqid() . $file->getClientOriginalName();
        
            $file->move($ruta, $fileName);

            $coord->foto = "/archivos_doc/imagenes/perfil/".$fileName;
        }
        $coord->save();
        $coord->assignRole($request->roles);
        $coord->circuitos()->attach($request->circuitos, ['es_responsable' => 2]);//con 2 indico que este circuito es propio de el


        $resp_circuitos = json_decode( $request->resp_circuitos );
        if(  count($resp_circuitos) > 0){
            foreach ($resp_circuitos as $key) {
                $coord->circuitos()->attach( $key->id , ['es_responsable' => 1]);
            }
        }
        $resp_localidades = json_decode( $request->resp_localidades );
        if(  count($resp_localidades) > 0){
            foreach ($resp_localidades as $key) {
                $coord->localidades()->attach( $key->id , ['es_responsable' => 1]);
            }
        }
        
        return redirect()->route('miembros.index')->with('success','Coordinador Cargado Correctamente');
    }

    public function show(Request $request,$id)
    {
        $coord = User::find($id);
        if ($coord->direccion_iddireccion != null) {
            $dir = Direccion::find($coord->direccion_iddireccion);
            $localidad = Localidad::find($dir->localidad_idlocalidad); 
            $dpto = Departamento::find($localidad->departamento_iddepartamento);
        } else {
            $dir = "sin direccion";
            $localidad = "sin localidad"; 
            $dpto = "sin departamento";
        }
        $resp_circ = $coord->circuitos()->wherePivot('es_responsable',1)->get();
        $resp_loc = $coord->localidades()->wherePivot('es_responsable',1)->get();
        
        if ($request->ajax()) {
            return response()->json([
                'usuario' => $coord,
                'direccion' => $dir,
                'localidad' => $localidad,
                'departamento' => $dpto,
                'respCircuitos' => $resp_circ,
                'respLocalidades' => $resp_loc,
                'status' => true
            ]);
        }else{
            return response()->json([
                'status' => false
            ]);
        }
    }

    public function edit($id)
    {
        $departamentos = Departamento::all();
        $localidades = Localidad::all();
        $circuitos = Circuito::all();
        $coord = User::find($id);
        $roles = Role::All();
        $dir = Direccion::find($coord->direccion_iddireccion);
        
        //dd($dir);
        if ($dir!= null) {
            $local = Localidad::find($dir->localidad_idlocalidad); 
            $dpto = Departamento::find($local->departamento_iddepartamento);
        } else {
            $local =null;
            $dpto =null;
        }
        
        return view('miembros.edit', compact('departamentos','localidades','circuitos','coord','dir','local','dpto','roles'));
    }

    public function update(Request $request, $id)
    {
        $validado = $this->validarRequest($request);
            if ($validado->fails()) {
                return back()
                            ->withErrors($validado)
                            ->withInput($request->all());
            } 
            $request->validate([
                'file' => 'image|mimes:jpeg,png,jpg,gif',
            ],[
                'file' => 'Debe seleccionar un archivo de tipo jpeg,png,jpg,gif',
            ]);
        $coord = User::find($id);
        if ($coord->direccion_iddireccion != null) {
            $d = Direccion::find($coord->direccion_iddireccion);
            $d->calle = $request->calle;
            $d->numero = $request->numero;
            $d->piso = $request->piso;
            $d->dpto = $request->dpto;
            $d->manzana = $request->manzana;
            $d->casa = $request->casa;  
            $d->localidad_idlocalidad = $request->localidad;
            $d->save();
        } else {
            $d = new Direccion();
            $d->calle = $request->calle;
            $d->numero = $request->numero;
            $d->piso = $request->piso;
            $d->dpto = $request->dpto;
            $d->manzana = $request->manzana;
            $d->casa = $request->casa;  
            $d->localidad_idlocalidad = $request->localidad;
            $d->save();
        }      

        $coord->nombre = $request->nombre;
        $coord->apellido = $request->apellido;
        $coord->dni = $request->dni;
        $coord->celular = $request->celular;
        $coord->telefono = $request->telefono;
        $coord->fecha_nacimiento = $request->fecha_nacimiento;
        $coord->email = $request->email;
        $coord->direccion_iddireccion = $d->iddireccion;
        $coord->estado = 1;
        if ($request->password != null) {
            $coord->password = Hash::make($request->password);
        }

        if ($request->file('file') != null) {
            if ( !Storage::disk('public')->exists("archivos_doc/imagenes/perfil")) {
                if(Storage::disk('public')->makeDirectory("/archivos_doc/imagenes/perfil" )){
                    //dd("creo la ruta");
                    $ruta = public_path()."/archivos_doc/imagenes/perfil";
                }else{                
                    //dd("No creo nada devolver algo un error ");
                    return redirect()->route('home')->with('warning','Coordinador Editado, "Foto no creada"');
                }            
            }else{
                //dd("entro al else ya existe la carpeta");
                $ruta = public_path()."/archivos_doc/imagenes/perfil";
            }
            if($coord->foto != null){
                $mi_imagen = public_path().$coord->foto;
                if (@getimagesize($mi_imagen)) {
                // echo "El archivo existe";
                    unlink($mi_imagen);
                }
            }
            $file = $request->file('file');
    
            $fileName = uniqid() . $file->getClientOriginalName();
        
            $file->move($ruta, $fileName);

            $coord->foto = "/archivos_doc/imagenes/perfil/".$fileName;
        }
        $coord->save();
        $coord->syncRoles($request->roles);

        $circuitoPropio = $coord->circuitos()->wherePivot('es_responsable',2)->get()->toArray();
        if($request->circuitos != $circuitoPropio[0]['idcircuito']){

            $coord->circuitos()->detach($circuitoPropio[0]['idcircuito']);
            $coord->circuitos()->attach([
                                $request->circuitos => ['es_responsable'=> 2]
                            ]);
        }

        // son los tags de responsabilidades
        $resp_circuitos =  json_decode( $request->resp_circuitos );      

        $circuitosActuales = $coord->circuitos()->wherePivot('es_responsable',1)->get()->toArray();
        $clave = 'idcircuito';
        $sync_circuitos = $this->conjuntoExcluyente($circuitosActuales,$resp_circuitos,$clave);
        // pos [0] = array de id actuales
        // pos [1] = array de id excluyentes
        foreach ($sync_circuitos[1] as $idCircuito) {
            if(in_array($idCircuito,$sync_circuitos[0])){
                $coord->circuitos()->detach($idCircuito);
            }else{
                $coord->circuitos()->attach([
                    $idCircuito => ['es_responsable'=> 1]
                ]);
            }
        }

        $resp_localidades = json_decode( $request->resp_localidades );
        $clave = 'idlocalidad';
        $localidadesActuales = $coord->localidades()->wherePivot('es_responsable',1)->get()->toArray();
        $sync_localidades = $this->conjuntoExcluyente($localidadesActuales,$resp_localidades,$clave);
        // pos [0] = array de id actuales
        // pos [1] = array de id excluyentes
        foreach ($sync_localidades[1] as $idLocadidad) {
            if(in_array($idLocadidad,$sync_localidades[0])){
                $coord->localidades()->detach($idLocadidad);
            }else{
                $coord->localidades()->attach([
                    $idLocadidad => ['es_responsable'=> 1]
                ]);
            }
        }
        //$coord->circuitos()->sync($request->circuitos);

        return redirect()->route('miembros.index')->with('success','Coordinador Editado Correctamente');
    }

    public function destroy($id)
    {
        $coord = User::find($id);
        $coord->estado = 2;
        $coord->save();
        return redirect()->route('miembros.index')->with('warning','Coordinador Eliminado correctamente');
    }
    public function validarRequest(Request $request)
    {
        $validado = Validator::make($request->all(), [
            'calle' => 'required',
            'numero'=>'required|numeric',
            'localidad'=>'required',
            'circuitos'=>'required',
            'departamento'=>'required',
            'nombre' => 'required',
            'apellido' => 'required',
            'dni'=>'required|numeric',
            'celular'=>'required|numeric',
            'fecha_nacimiento' => 'required',
            'email' => 'required',
            'roles'=>'required',
            //'file' => 'mimes:jpeg,png,jpg,gif',
        ],[
            'calle' => 'La calle es requerida',
            'numero'=>'El numero es requerido',
            'localidad'=>'La localidad es requerido',
            'circuitos'=>'El circuito es requerido',
            'departamento'=>'El departamento es requerido',
            'nombre' => 'El nombre es requerido',
            'apellido' => 'El apellido es requerido',
            'dni'=>'El DNI es requerido',
            'celular'=>'El celular es requerido',
            'fecha_nacimiento' => 'La fecha es requerida',
            'email' => 'El Correo es requerido',
            'roles'=>'El rol es requerido',
            //'file' => 'el archivo debe ser de tipo jpeg,png o jpg',
        ]);
        return $validado;

    }

    public function conjuntoExcluyente($elementosActuales,$elementosEdit,$clave)
    {
        $arrays = [];
        $ids_actuales = array_map(function ($elem) use ($clave)
        {
           return $elem[$clave];
        },$elementosActuales);

        $ids_edit = array_map(function ($elem)
        {
           return intval($elem->id);
        },$elementosEdit);

        $interseccion = array_uintersect($ids_actuales,$ids_edit,"strcasecmp");
       // $ids_sin_repetir = array_unique(array_merge($ids_actuales,$ids_edit));
        
        $union_ids = array_unique(array_merge($ids_actuales,$ids_edit));
        $ids_excluyentes = array_filter($union_ids,function ($elem) use ($interseccion)
        {
            return !in_array($elem, $interseccion);
        });
       // dd($union_ids);
        array_push($arrays,$ids_actuales);
        array_push($arrays,$ids_excluyentes);
        return $arrays;
        
    }
}

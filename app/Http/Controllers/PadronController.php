<?php

namespace App\Http\Controllers;

use App\Circuito;
use App\Padron;
use App\Voluntario;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class PadronController extends Controller
{
    public function index()
    {
       
       $array_padron = Padron::select("count(*) as")->count();
        /* $array_padron = DB::table('padron')
        ->select('idvoluntario','padron.*')
            ->leftJoin('voluntarios', 'voluntarios.dni', '=', 'padron.dni')
            ->get(); */

        //$array_padron = DB::table('padron')->paginate(20);

        /* $array_padron = DB::select("SELECT 
        v.idvoluntario, padron.*
        FROM
        padron
            LEFT JOIN
        voluntarios v ON v.dni = padron.dni
        where 1= 1"); */
       // dd($array_padron);
        return view('padrones.index',compact('array_padron'));        
    }
    public function listAjax(Request $request)
    {
        $draw = $request->get('draw');
        $start = $request->get('start');
        $rowperpage = $request->get('length');

        $columnIndex_arr = $request->get('order');
        $columnName_arr = $request->get('columns');
        $order_arr = $request->get('order');
        $search_arr = $request->get('search');

        $columnIndex = $columnIndex_arr[0]['column'];
        $columnName = $columnName_arr[$columnIndex]['data'];
        $columnSortOrder = $order_arr[0]['dir'];
        $searchValue = $search_arr['value'];

        //Total Records
        $totalRecords = Padron::select("count(*) as allcount")->count();

        // Total records with search filter
        $totalRecordswithFilter = Padron::select('count(*) as allcount')
                                    ->where('dni','like','%'.$searchValue)
                                    ->count();
        
        // Fetch records
        $records = Padron::orderBy($columnName,$columnSortOrder)
                            ->where('padron.dni','like','%'.$searchValue.'%')
                            ->skip($start)
                            ->take($rowperpage)
                            ->get();
       // $voluntarios = Voluntario::All();

        $data_arr = array();
        foreach ($records as $record) {
            $idpadron = $record->idpadron;
            $dni = $record->dni;

            $voluntario = Voluntario::where('dni',$dni)->get()->count();
           // foreach ($voluntarios as $val) {
                if( $voluntario > 0){
                    $isvoluntario = '<span class="badge badge-pill badge-primary">Es voluntario</span>';
                }else{
                    $isvoluntario = '';
                }
           // }
            
            
            $tipo_ejemplar = $record->tipo_ejemplar;
            $NomApe = $record->apellido.','.$record->Nombre.
                        '<br> 
                        <span class="badge badge-pill badge-success">DNI: '.$dni.'</span>
                        <br> '.$isvoluntario;
            $domicilio = $record->domicilio;
            $Circuito2 = $record->Circuito2;
            $Localidad = $record->Localidad;
            $Departamento = $record->Departamento;
            $Seccion = $record->Seccion;
            $sexo = $record->sexo;
            $mesa = $record->mesa;
            $orden_mesa = $record->orden_mesa;
            $establecim = $record->establecim;
            $domic_establec = $record->domic_establec;
            /* $btnEdit = ' <a href="{{route(\'padron.edit\','.$idpadron.')}}" class="editpadron" id="edit_'.$idpadron.'"><i class="far fa-edit fa-2x"></i></a>';
            $btnDelete = '<a href="javascript:;"
                            onclick="eliminarpadron('.$idpadron.');" ><i class="fas fa-times fa-2x"></i></a>
                        <form id="delete-form_'.$idpadron.'" action="{{ route(\'padron.destroy\','.$idpadron.') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                            {{ method_field(\'DELETE\') }}
                        </form>'; */

            $data_arr[] = array(
                //"idpadron" => $idpadron,
                "tipo_ejemplar" => $tipo_ejemplar,
                "NomApe" => $NomApe,
                "domicilio" => $domicilio,
                "Circuito2" => $Circuito2,
                "Localidad" => $Localidad,
                "Departamento" => $Departamento,
                "Seccion" => $Seccion,
                "sexo" => $sexo,
                "mesa" => $mesa,
                "orden_mesa" => $orden_mesa,
                "establecim" => $establecim,
                "domic_establec" => $domic_establec
                //"btnEdit" => $btnEdit,
                //"btnDelete" => $btnDelete
            ); 
        }

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordswithFilter,
            "aaData" => $data_arr
        );

        echo json_encode($response);
        exit;

        //dd($request);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $request->validate([
            'padronFile' => 'required|mimes:pdf',
        ],[
            'padronFile' => 'Debe seleccionar un archivo de tipo pdf',
        ]);
      
    }

    public function show(Padron $p)
    {
        //
    }

    public function edit(Padron $p)
    {
        //
    }

    public function update(Request $request, Padron $p)
    {
        //
    }

    public function destroy(Padron $p)
    {
        //
    }
}

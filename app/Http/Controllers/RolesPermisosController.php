<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesPermisosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::all();
       // dd($roles);
        return view('roles_permisos.index',compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permisos = Permission::all();
        return view('roles_permisos.create',compact('permisos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rol = Role::create(['name' => $request->rol]);
        $permisos = json_decode( $request->permisos );
        if(  count($permisos) > 0){
            $arrP = array();
            foreach ($permisos as $key) {
                array_push($arrP, $key->id);
            }
            //$rol->permissions()->sync($arrP);
            $rol->syncPermissions($arrP);
            //$rol->syncPermissions($arrP);
        }
        return redirect()->route('roles-permisos.index')->with('success','Rol creado Correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $permisos = Permission::all();
        $rol = Role::find($id);
       // dd($permisos);
        return view('roles_permisos.edit',compact('permisos','rol'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rol = Role::find($id);
        $permisos = json_decode( $request->permisos );
        if(  count($permisos) > 0){
            $arrP = array();
            foreach ($permisos as $key) {
                array_push($arrP, $key->id);
            }
            //$rol->permissions()->sync($arrP);
            $rol->syncPermissions($arrP);
            //$rol->syncPermissions($arrP);
        }
        return redirect()->route('roles-permisos.index')->with('success','Permisos Asignados Correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rol = Role::find($id);
        $rol->delete();
        return redirect()->route('roles-permisos.index')->with('success','Rol eliminado Correctamente');
    }
}

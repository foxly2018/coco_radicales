<?php

namespace App\Http\Controllers;

use App\Circuito;
use App\Departamento;
use App\Direccion;
use App\Documento;
use App\Localidad;
use App\Voluntario;
use App\TipoInteraccion;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class LlamadasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        
            $voluntarios = DB::table('voluntarios')
            ->join('circuitos','voluntarios.circuito_idcircuito','circuitos.idcircuito')
            ->join('localidades','circuitos.localidad_idlocalidad','localidades.idlocalidad')
            ->join('interacciones','voluntarios_idvoluntario','idvoluntario')
         ->join('tipo_interaccion','tipo_interaccion.id','tipo_interaccion_id')
         ->join('users', 'interacciones.users_id', 'users.id')
           ->select('voluntarios.*','interacciones.id as inte_id','descripcion','fecha','tipo_interaccion.tipo_nombre','calidad', 'users.nombre as usernombre', 'users.apellido as userapellido')
            ->where('voluntarios.estado',1)->orderBy('voluntarios.circuito_idcircuito', 'asc')
            ->get();//Voluntario::all()->where('estado',1)->orderBy('circuitos.circuito_nombre', 'DESC')->get();
         
            $fechaHoy = date('Y-m-d');
            //$contador=0;
            $contador = DB::table('interacciones')->where('fecha', '>=', $fechaHoy)->count();
       /*  if (auth()->user()->hasRole('Administrador')) {
           $voluntarios = DB::table('voluntarios')
           ->join('circuitos','voluntarios.circuito_idcircuito','circuitos.idcircuito')
           ->join('localidades','circuitos.localidad_idlocalidad','localidades.idlocalidad')
           ->join('interacciones','voluntarios_idvoluntario','idvoluntario')
        ->join('tipo_interaccion','tipo_interaccion.id','tipo_interaccion_id')
          // ->select('voluntarios.*')
          ->select('voluntarios.*','interacciones.id as inte_id','descripcion','fecha','tipo_interaccion.tipo_nombre')
           ->where('voluntarios.estado',1)->orderBy('voluntarios.circuito_idcircuito', 'asc')
           ->get();//Voluntario::all()->where('estado',1)->orderBy('circuitos.circuito_nombre', 'DESC')->get();
        }  */
       
       
    //dd($voluntarios);
        return view('llamadas.index',compact('voluntarios','fechaHoy','contador'));
    }
    
    public function contador()
    {
         
        $fechaHoy = date('Y-m-d');
        $contador=0;
        $contador = DB::table('interacciones')->where('fecha', '<=', $fechaHoy)->count()->fisrt();

        return $contador;
       // return view('llamadas.index',$fechaHoy);

       /*  $users = User::withCount([
            'posts', 
            'comments', 
            'comments as approved_comments_count' => function ($query) {
                $query->where('approved', 1);
            }])
            ->get(); */

       /*  $contador = DB::table('voluntarios')
        ->join('circuitos','voluntarios.circuito_idcircuito','circuitos.idcircuito')
        ->join('localidades','circuitos.localidad_idlocalidad','localidades.idlocalidad')
        ->join('interacciones','voluntarios_idvoluntario','idvoluntario')
     ->join('tipo_interaccion','tipo_interaccion.id','tipo_interaccion_id')
     ->join('users', 'interacciones.users_id', 'users.id')
       ->select('fecha')
        ->where('fecha', '<=', $fechaHoy)->count()
        ->get(); */
    
    
        //return view('llamadas.index',$contador);
    }

    public function create()
    {
        
    }

    public function store(Request $request)
    {
        
    }
    
    public function storeInteraccion(Request $request)
    {
    }
    public function show(Request $request,$id)
    {
        $v = Voluntario::find($id);
        $dir = Direccion::find($v->direccion_iddireccion);
        $localidad = Localidad::find($dir->localidad_idlocalidad); 
        $dpto = Departamento::find($localidad->departamento_iddepartamento);
        $tipoInteraccion = TipoInteraccion::All();
      //  dd($v->interacciones);
        if ($request->ajax()) {
            return response()->json([
                'voluntario' => $v,
                'direccion' => $dir,
                'localidad' => $localidad,
                'departamento' => $dpto,
                'tipoInteraccion'=>$tipoInteraccion,
                'interacciones' => $v->interacciones,
                'status' => true
            ]);
        }else{
            return response()->json([
                'status' => false
            ]);
        }
    }

    public function edit($id)
    {
       
    }

    public function update(Request $request, $id)
    {
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }
    
    public function imprimirInteracciones(Request $request,$id)
    {
        $v = Voluntario::find($id);
        $interacciones = DB::table('voluntarios')
        ->select('interacciones.id as inte_id','descripcion','fecha','tipo_interaccion.tipo_nombre')
        ->join('interacciones','voluntarios_idvoluntario','idvoluntario')
        ->join('tipo_interaccion','tipo_interaccion.id','tipo_interaccion_id')
        ->where('idvoluntario', '=', $request->id_volutario_imprimir)
        ->get();

        return view('voluntarios.reporteIntereccion', compact('interacciones'));
    }
    public function ajaxValidarDni(Request $request)
    {
        if ($request->ajax()) {
            $comprobarDNI = $request->dni;
            $voluntarios = DB::table('voluntarios')
            ->select('dni')
            ->where('dni', '=', $comprobarDNI)
            ->get();
            if(count($voluntarios) >= 1){
                return response()->json([
                    'mjs' => 'El Voluntario ya existe, intente de nuevo con un DNI distinto',
                    'status' => false
                ]);
            }
            else{
                return response()->json([
                    'mjs' => 'valido',
                    'status' => true
                ]);
            }

            
        }
    }

    public function validarRequest(Request $request)
    {
        $validado = Validator::make($request->all(), [
            'calle' => 'required',
            'numero'=>'required|numeric',
            'localidad'=>'required',
            'circuitos'=>'required',
            'departamento'=>'required',
            'nombre' => 'required',
            'apellido' => 'required',
            'dni'=>'required|numeric',
            'celular'=>'required|numeric',
            'fecha_nacimiento' => 'required',
            //'file' => 'mimes:jpeg,png,jpg,gif',
        ],[
            'calle' => 'La calle es requerida',
            'numero'=>'El numero es requerido',
            'localidad'=>'La localidad es requerido',
            'circuitos'=>'El circuito es requerido',
            'departamento'=>'El departamento es requerido',
            'nombre' => 'El nombre es requerido',
            'apellido' => 'El apellido es requerido',
            'dni'=>'El DNI es requerido',
            'celular'=>'El celular es requerido',
            'fecha_nacimiento' => 'La fecha es requerida',
            //'file' => 'el archivo debe ser de tipo jpeg,png o jpg',
        ]);
        return $validado;

    }
}

<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

   // use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest')->except('logout');
        $this->middleware('guest',['only'=>'showLoginForm']);
    }
    public function login(Request $request)
    {
        $credentials = $this->validate(request(),[
            'email' => 'email|required|string',
            'password' => 'required|string'
        ]);
            
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'estado' => 1]))
        {       
            $u = auth()->user();
            return redirect()->route('home');
           // return view('home.index',compact('u'));
        }
        else{
            return back()
            ->withErrors(['email'=> trans('auth.failed')])
            ->withInput(request(['email']));
        }

        
    }
    public function logout(Request $request)
    {
        Auth::logout();
        return redirect()->route('login_index');
    }
}

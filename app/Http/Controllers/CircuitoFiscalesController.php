<?php

namespace App\Http\Controllers;

use App\CircuitoFiscal;
use Illuminate\Http\Request;

class CircuitoFiscalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $circuitoFiscal= DB::table('circuito_fiscales')
        ->join('localidades','circuito_fiscales.localidad_idlocalidad','localidades.idlocalidad')
                        ->select('circuitos.*')
                        ->where('circuito_fiscales.disponible','si')
                        ->get();
       
        return view('circuitoFiscal.index',compact('circuitoFiscal'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CircuitoFiscal  $circuitoFiscal
     * @return \Illuminate\Http\Response
     */
    public function show(CircuitoFiscal $circuitoFiscal)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CircuitoFiscal  $circuitoFiscal
     * @return \Illuminate\Http\Response
     */
    public function edit(CircuitoFiscal $circuitoFiscal)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CircuitoFiscal  $circuitoFiscal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CircuitoFiscal $circuitoFiscal)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CircuitoFiscal  $circuitoFiscal
     * @return \Illuminate\Http\Response
     */
    public function destroy(CircuitoFiscal $circuitoFiscal)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use App\Circuito;
use App\Departamento;
use App\Direccion;
use App\Documento;
use App\Localidad;
use App\Voluntario;
use App\TipoInteraccion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class VoluntariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if (auth()->user()->hasRole('Administrador')) {

            $circuitos = Circuito::all();
            
            if (isset($request->circuitos_seleccionados)) {

                $resp_circuitos = json_decode( $request->circuitos_seleccionados );

                $voluntarios = DB::table('voluntarios')
                ->join('circuitos','voluntarios.circuito_idcircuito','circuitos.idcircuito')
                ->join('localidades','circuitos.localidad_idlocalidad','localidades.idlocalidad')
                ->join('direcciones','voluntarios.direccion_iddireccion','direcciones.iddireccion')
                ->select( DB::raw('concat(calle," N°: ",numero) as dir, voluntarios.*'))
                ->where('voluntarios.estado',1)
                ->where(function($query) use ($resp_circuitos)
                        {
                            foreach ($resp_circuitos  as $c) {
                                    $query->orWhere('circuitos.idcircuito',$c->id);
                            }                                
                        })->orderBy('voluntarios.circuito_idcircuito', 'asc')
                ->get();
            } else {
                $voluntarios = DB::table('voluntarios')
                ->join('circuitos','voluntarios.circuito_idcircuito','circuitos.idcircuito')
                ->join('localidades','circuitos.localidad_idlocalidad','localidades.idlocalidad')
                ->join('direcciones','voluntarios.direccion_iddireccion','direcciones.iddireccion')
                ->select( DB::raw('concat(calle," N°: ",numero) as dir, voluntarios.*'))
                ->where('voluntarios.estado',1)->orderBy('voluntarios.circuito_idcircuito', 'asc')
                ->get();//Voluntario::all()->where('estado',1)->orderBy('circuitos.circuito_nombre', 'DESC')->get();
            }
        } else {
            //$permissionNames = auth()->user()->getAllPermissions();

            $circuitos = auth()->user()->circuitos()->wherePivot('es_responsable',1)->get();
            $localidades = auth()->user()->localidades()->wherePivot('es_responsable',1)->get();

            $voluntarios = DB::table('voluntarios')
                        ->join('circuitos','voluntarios.circuito_idcircuito','circuitos.idcircuito')
                        ->join('localidades','circuitos.localidad_idlocalidad','localidades.idlocalidad')
                        ->join('direcciones','voluntarios.direccion_iddireccion','direcciones.iddireccion')
                        ->select( DB::raw('concat(calle," N°: ",numero) as dir, voluntarios.*'))
                        ->where('voluntarios.estado',1)
                        ->where(function($query) use ($circuitos , $localidades, $request)
                        {
                            $b = false;
                            foreach ($circuitos  as $c) {
                                $b = true;
                                if (isset($request->circuitos_seleccionados)) {
                                    $resp_circuitos = json_decode( $request->circuitos_seleccionados );
                                    foreach ($resp_circuitos  as $filtro) {
                                        if ($filtro->id == $c->idcircuito) {
                                            $query->orWhere('circuitos.idcircuito',$filtro->id);
                                        }
                                    }
                                } else {
                                    $query->orWhere('circuitos.idcircuito','=', $c->idcircuito);
                                }
                                
                            }
                            foreach ($localidades as $l) {
                                $b = true;
                                $query->orWhere('localidades.idlocalidad','=', $l->idlocalidad);
                            }
                            if (!$b) {
                                $query->Where('circuitos.circuito_nombre','=', '');
                            }
                        })->orderBy('voluntarios.circuito_idcircuito', 'asc')
                        ->get();
        }
        

   // dd($circuitos);
        return view('voluntarios.index',compact('voluntarios','circuitos'));
    }

    public function create()
    {
        $departamentos = Departamento::all();
        $localidades = Localidad::all();
        $circuitos = Circuito::all();
        $Amin = date ( 'Y' , strtotime ( '-90 year' , strtotime ( date('Y') ) ) ) ;
        $Amax = date ( 'Y' , strtotime ( '-16 year' , strtotime ( date('Y') ) ) ) ;
        return view('voluntarios.create', compact('departamentos','localidades','circuitos','Amin','Amax'));
    }

    public function store(Request $request)
    {
        $validado = $this->validarRequest($request);
            if ($validado->fails()) {
                return back()
                            ->withErrors($validado)
                            ->withInput($request->all());
            }
        $request->validate([
            'file' => 'image|mimes:jpeg,png,jpg,gif',
          ],[
            'file' => 'Debe seleccionar un archivo de tipo jpeg,png,jpg,gif',
        ]);
        $comprobarDNI = $request->dni;
        $voluntarios = DB::table('voluntarios')
        ->select('dni')
        ->where('dni', '=', $comprobarDNI)
        ->get();
        if(count($voluntarios) >= 1){
            return back()->withInput($request->all())
            ->with('warning','El Voluntario ya existe, intente de nuevo con un DNI distinto');
        }
        else{
            $d = new Direccion();
            $d->calle = $request->calle;
            $d->numero = $request->numero;
            $d->piso = $request->piso;
            $d->dpto = $request->dpto;
            $d->manzana = $request->manzana;
            $d->casa = $request->casa;  
            $d->localidad_idlocalidad = $request->localidad;
            $d->save();
    
            $v = new Voluntario();
            $v->nombre = $request->nombre;
            $v->apellido = $request->apellido;
            $v->dni = $request->dni;
            $v->celular = $request->celular;
            $v->telefono = $request->telefono;
            $v->fecha_nacimiento = $request->fecha_nacimiento;
            $v->email = ($request->email != null )?$request->email:'';
            $v->direccion_iddireccion = $d->iddireccion;
            $v->circuito_idcircuito = $request->circuitos;
            $v->estado = 1;
            if($request->origen != null){
                $v->origen = $request->origen;
            }

            if ($request->file('file') != null) {
                if ( !Storage::disk('public')->exists("archivos_doc/imagenes/perfil")) {
                    if(Storage::disk('public')->makeDirectory("/archivos_doc/imagenes/perfil" )){
                        //dd("creo la ruta");
                        $ruta = public_path()."/archivos_doc/imagenes/perfil";
                    }else{                
                        //dd("No creo nada devolver algo un error ");
                        return redirect()->route('home')->with('warning','Voluntario Creado, "Foto no creada"');
                    }            
                }else{
                    //dd("entro al else ya existe la carpeta");
                    $ruta = public_path()."/archivos_doc/imagenes/perfil";
                }
                $file = $request->file('file');
        
                $fileName = uniqid() . $file->getClientOriginalName();
            
                $file->move($ruta, $fileName);
    
                $v->foto = "/archivos_doc/imagenes/perfil/".$fileName;
            }
            $v->save();
            
            return redirect()->route('home')->with('success','Voluntario Cargado Correctamente');
        }

    }
    
    public function storeInteraccion(Request $request)
    {
        $mjs = "Interaccion creada Correctamente";
        $v = Voluntario::find($request->voluntarios_idvoluntario);
        if($request->confirmarInteraccion == 1){
            $v->confirmarInteraccion = $request->confirmarInteraccion;
            $v->calidadInteraccion = $request->calidadInteraccion;
            $v->serFiscal = $request->serFiscal;
            if($request->calidadInteraccion == 2){
                $v->cantNegativas = ($v->cantNegativas != null)? $v->cantNegativas + 1: 1;
            }else{
                $v->cantNegativas = 0;
            }
           

            $v->interacciones()->attach(
                auth()->user()->id,
                [
                    'descripcion' => $request->interaccionDescriptcion,
                    'fecha' => date('Y-m-d h:m:s'),
                    'estado'=>1,
                    'tipo_interaccion_id'=>$request->interaccionTipo,
                    'calidad'=>$request->calidadInteraccion,
                    'created_at'=>date('Y-m-d h:m:s'),
                    'updated_at'=>date('Y-m-d h:m:s')
                ]
            );
        }else{
            $v->confirmarInteraccion = $request->confirmarInteraccion;
            $mjs = "No hubo Interaccion :( suerte para la proxima";
        }
        $v->save();
        $tipoInteraccion = TipoInteraccion::All();
        if ($request->ajax()) {
            return response()->json([
                'interacciones' => $v->interacciones,
                'tipoInteraccion'=>$tipoInteraccion,
                'mjs'=>$mjs,
                'status' => true
            ]);
        }else{
            return response()->json([
                'mjs'=>'Algo salio mal, por favor intente de nuevo',
                'status' => false
            ]);
        }
    }
    public function show(Request $request,$id)
    {
        $v = Voluntario::find($id);
        $dir = Direccion::find($v->direccion_iddireccion);
        $localidad = Localidad::find($dir->localidad_idlocalidad); 
        $dpto = Departamento::find($localidad->departamento_iddepartamento);
        $tipoInteraccion = TipoInteraccion::All();
      //  dd($v->interacciones);
        if ($request->ajax()) {
            return response()->json([
                'voluntario' => $v,
                'direccion' => $dir,
                'localidad' => $localidad,
                'departamento' => $dpto,
                'tipoInteraccion'=>$tipoInteraccion,
                'interacciones' => $v->interacciones,
                'status' => true
            ]);
        }else{
            return response()->json([
                'status' => false
            ]);
        }
    }

    public function edit($id)
    {
        $departamentos = Departamento::all();
        $localidades = Localidad::all();
        $circuitos = Circuito::all();
        $v = Voluntario::find($id);
        $dir = Direccion::find($v->direccion_iddireccion);
        $local = Localidad::find($dir->localidad_idlocalidad); 
        $dpto = Departamento::find($local->departamento_iddepartamento);
        $Amin = date ( 'Y' , strtotime ( '-90 year' , strtotime ( date('Y') ) ) ) ;
        $Amax = date ( 'Y' , strtotime ( '-16 year' , strtotime ( date('Y') ) ) ) ;
        return view('voluntarios.edit', compact('departamentos','localidades','circuitos','v','dir','local','dpto','Amin','Amax'));

    }

    public function update(Request $request, $id)
    {
        $validado = $this->validarRequest($request);
            if ($validado->fails()) {
                return back()
                            ->withErrors($validado)
                            ->withInput($request->all());
            } 
            $request->validate([
                'file' => 'image|mimes:jpeg,png,jpg,gif',
            ],[
                'file' => 'Debe seleccionar un archivo de tipo jpeg,png,jpg,gif',
            ]);
        $v = Voluntario::find($id);
        $d = Direccion::find($v->direccion_iddireccion);
        $d->calle = $request->calle;
        $d->numero = $request->numero;
        $d->piso = $request->piso;
        $d->dpto = $request->dpto;
        $d->manzana = $request->manzana;
        $d->casa = $request->casa;  
        $d->localidad_idlocalidad = $request->localidad;
        $d->save();

        $v->nombre = $request->nombre;
        $v->apellido = $request->apellido;
        $v->dni = $request->dni;
        $v->celular = $request->celular;
        $v->telefono = $request->telefono;
        $v->fecha_nacimiento = $request->fecha_nacimiento;
        $v->email = ($request->email != null )?$request->email:'';
        $v->circuito_idcircuito = $request->circuitos;
        $v->estado = 1;
        if($request->origen != null){
            $v->origen = $request->origen;
        }

        if ($request->file('file') != null) {
            if ( !Storage::disk('public')->exists("archivos_doc/imagenes/perfil")) {
                if(Storage::disk('public')->makeDirectory("/archivos_doc/imagenes/perfil" )){
                    //dd("creo la ruta");
                    $ruta = public_path()."/archivos_doc/imagenes/perfil";
                }else{                
                    //dd("No creo nada devolver algo un error ");
                    return redirect()->route('home')->with('warning','Voluntario Editado, "Foto no creada"');
                }            
            }else{
                //dd("entro al else ya existe la carpeta");
                $ruta = public_path()."/archivos_doc/imagenes/perfil";
            }
            if($v->foto != null){
                $mi_imagen = public_path().$v->foto;
                if (@getimagesize($mi_imagen)) {
                // echo "El archivo existe";
                    unlink($mi_imagen);
                }
            }
            $file = $request->file('file');
    
            $fileName = uniqid() . $file->getClientOriginalName();
        
            $file->move($ruta, $fileName);

            $v->foto = "/archivos_doc/imagenes/perfil/".$fileName;
        }
        $v->save();

        return redirect()->route('home')->with('success','Voluntario Editado Correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $v = Voluntario::find($id);
        $v->estado = 2;
        $v->save();
        return redirect()->route('home')->with('warning','Voluntario Eliminado correctamente');
    }
    
    public function imprimirInteracciones(Request $request,$id)
    {
        $v = Voluntario::find($id);
        $interacciones = DB::table('voluntarios')
        ->select('interacciones.id as inte_id','descripcion','fecha','tipo_interaccion.tipo_nombre')
        ->join('interacciones','voluntarios_idvoluntario','idvoluntario')
        ->join('tipo_interaccion','tipo_interaccion.id','tipo_interaccion_id')
        ->where('idvoluntario', '=', $request->id_volutario_imprimir)
        ->get();

        return view('voluntarios.reporteIntereccion', compact('interacciones'));
    }
    public function ajaxValidarDni(Request $request)
    {
        if ($request->ajax()) {
            $comprobarDNI = $request->dni;
            $voluntarios = DB::table('voluntarios')
            ->select('dni')
            ->where('dni', '=', $comprobarDNI)
            ->get();
            if(count($voluntarios) >= 1){
                return response()->json([
                    'mjs' => 'El Voluntario ya existe, intente de nuevo con un DNI distinto',
                    'status' => false
                ]);
            }
            else{
                return response()->json([
                    'mjs' => 'valido',
                    'status' => true
                ]);
            }

            
        }
    }

    public function validarRequest(Request $request)
    {
        $validado = Validator::make($request->all(), [
            'calle' => 'required',
            'numero'=>'required|numeric',
            'localidad'=>'required',
            'circuitos'=>'required',
            'departamento'=>'required',
            'nombre' => 'required',
            'apellido' => 'required',
            'dni'=>'required|numeric',
            'celular'=>'required|numeric',
            'fecha_nacimiento' => 'required',
            //'file' => 'mimes:jpeg,png,jpg,gif',
        ],[
            'calle' => 'La calle es requerida',
            'numero'=>'El numero es requerido',
            'localidad'=>'La localidad es requerido',
            'circuitos'=>'El circuito es requerido',
            'departamento'=>'El departamento es requerido',
            'nombre' => 'El nombre es requerido',
            'apellido' => 'El apellido es requerido',
            'dni'=>'El DNI es requerido',
            'celular'=>'El celular es requerido',
            'fecha_nacimiento' => 'La fecha es requerida',
            //'file' => 'el archivo debe ser de tipo jpeg,png o jpg',
        ]);
        return $validado;

    }
}

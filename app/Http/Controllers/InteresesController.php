<?php

namespace App\Http\Controllers;

use App\Intereses;
use Illuminate\Http\Request;

class InteresesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Intereses  $intereses
     * @return \Illuminate\Http\Response
     */
    public function show(Intereses $intereses)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Intereses  $intereses
     * @return \Illuminate\Http\Response
     */
    public function edit(Intereses $intereses)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Intereses  $intereses
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Intereses $intereses)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Intereses  $intereses
     * @return \Illuminate\Http\Response
     */
    public function destroy(Intereses $intereses)
    {
        //
    }
}

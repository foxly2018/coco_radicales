<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Padron extends Model
{
    protected $table = 'padron';
    protected $primaryKey = 'idpadron';
    public $timestamps = false;

    protected $fillable = [
        'idpadron',
        'dni',
        'tipo_ejemplar',
        'apellido',
        'Nombre',
        'domicilio',
        'Circuito2',
        'Localidad',
        'Departamento',
        'Seccion',
        'sexo',
        'mesa',
        'orden_mesa',
        'establecim',
        'domic_establec',
        'local_establec',
        'depart_establec'
    ];
}

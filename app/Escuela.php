<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Escuela extends Model
{
    protected $table = 'escuelas';
    protected $primaryKey = 'idescuela';

    protected $fillable = [
        'nombre_escuela','circuitofiscal','numero_mesas','faltan_mesas','direccion','disponible','estado','comida','fiscal_general'
    ];

    
     //1:1
     public function circuitofiscal()
     {
         return $this->hasOne('App\CircuitoFiscal','idcircuitofiscal','circuitofiscal');
     }

     public function fiscal_general()
     {
         return $this->hasOne('App\User','id','fiscal_general');
     }

     //1:N 
    public function mesas()
    {
        return $this->hasMany('App\Mesa','id_escuela','idescuela');
       
    }
}

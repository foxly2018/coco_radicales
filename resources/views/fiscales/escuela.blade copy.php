@extends('layout')
@php
    
    use App\CircuitoFiscal;
    use App\Escuela;
    use App\Mesa;
    use App\Fiscal;
    use App\Voluntario;
    use App\Circuito;

@endphp
@section('title', 'Fiscales')

@section('seccionSaludo')
<h1>Listado de Fiscales</h1>
{{-- <h3 class="title text-center">---</h3> --}}
@endsection

@section('contenido')
    <div class="col-md-12">

        <div class="row">
        {{-- @can('fiscal-alta') --}}
                <a href="{{route('fiscal.alta')}}"; class="btn btn-success" style="margin-left: 89%;">Nuevo Fiscal</a>
                {{--    @endcan --}}
        </div>

        <div class="table-responsive" id="seccionTabla">
            <table class="table table-striped table-dark table-sm" id="tableFiscales" class="display compact" style="width:100%">
                <thead class="thead-dark">
                <tr>
                    <th>Nombre y Apellido</th>
                    <th>Celular</th>
                    <th>Circuito</th>
                    <th>Escuela</th>
                    <th>Mesa</th>
                    {{-- <th>Celular</th> --}}
                </tr>
                </thead>
                <tbody>
                    @foreach($fiscales as $fiscal)  
                        <tr>
                            <td>
                                {{ $fiscal->apellido .','.$fiscal->nombre}}                                
                            </td>
                            <td>{{ $fiscal->celular}}</td> --}}
                            <td>{{ $fiscal->circuito}}</td>
                            <td>{{ $fiscal->escuela}}</td>
                            <td>{{ $fiscal->mesa}}</td>
                            <td>
                                @can('fiscal-detalle')                                    
                                <a href="javascript:;" class="showFiscal" id="show_{{ $fiscal->idfiscales}}"><i class="far fa-eye fa-2x"></i></a>                            
                                @endcan                          
                            </td>
                            <td>
                                @can('fiscal-edicion')                                    
                                <a href="{{route('fiscal.edit',$fiscal->idfiscales)}}" class="editFiscal" id="edit_{{ $fiscal->idfiscales}}"><i class="far fa-edit fa-2x"></i></a>
                                @endcan
                            </td>
                            <td>
                                @can('fiscal-eliminar')
                                <a href="javascript:;"
                                    onclick="eliminarfiscal({{ $fiscal->idfiscales}});"><i class="fas fa-times fa-2x"></i></a>
                                <form id="delete-form_{{ $fiscal->idfiscal}}" action="{{ route('fiscal.destroy',$fiscal->idfiscales) }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                </form>                                     
                                @endcan
                                
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

        </div>

        
        <div id="ajaxExito" class="alert alert-success message" style="display:none;bottom: 7%;text-align:center;width:80%;z-index: 1000;position: fixed;">

        </div>
        <div id="ajaxError" class="alert alert-danger message" style="display:none;bottom: 7%;text-align:center;width:80%;z-index: 1000;position: fixed;">
            
        </div>
    </div>

@endsection

@section('script')
<script>
var tableInteracciones;
$(document).ready(function() {
    setTimeout(function() {
      $('#message').fadeOut('fast');
    }, 3500);

    const TVoluntarios = $('#tableVoluntarios').DataTable({
         dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'pdfHtml5',
        ], 
        "order": [],
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
        },
        // pageLength : 5,
        //lengthMenu: [[10, 20, -1], [10, 20, 'Todos']],
        
    });
    TVoluntarios.page.len(20).draw();
    tableInteracciones = $('#tablaInteracciones').DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
        },
        // pageLength : 5,
        lengthMenu: [[5,10, 20, -1], [5,10, 20, 'Todos']],
        
    });
} );

});


$(document).on('click','#volverList',function(){
    $('#seccionDatos').hide('slow');
    $('#seccionTabla').show('slow');
});

function eliminarFiscal(idfiscales){
    if(confirm('Esta acción no podrá deshacerse. ¿Continuar?')){

        document.getElementById('delete-form_'+idfiscales).submit();
    }
}
/* EMERGENTES */
function mjsAlert(id){
    $(id).show('slow');
    setTimeout(function() {
    $('.message').hide('slow');
    }, 3500);
}// FIN EMERGENTES //
</script>
@endsection

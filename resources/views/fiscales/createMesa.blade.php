@extends('layout')

@section('title', 'Alta Fiscales')

@section('seccionSaludo')
<h1>Nuevo Fiscal</h1>
<h2> {{$escuelaactual->nombre_escuela}} </h2>
@endsection

@section('contenido')

    <div class="col-md-12">
        <div class="row">
            <a href="{{route('fiscales.index')}}" class="btn btn-dark float-left" style="margin-bottom: 3%;">Volver</a>
        </div>
    {{--         <div class="text-center">
            <h3 class="title">Nuevo Fiscal</h3>
            
        </div> --}}
        <form id="fiscalMesa_store" action="{{ route('fiscal.storeMesa') }}" method="POST" enctype="multipart/form-data">
            @csrf
            
            <div class="row">
            <div class="form-group">
            <input type="hidden" name="escuela" value="{{$escuelaactual->idescuela}}" id="escuela">
            </div>
                <div class="col-lg-6 col-sm-4">
                    <div class="form-group">
                    <label>Voluntario:</label>
                    {{-- <input type="hidden" value="{{$voluntarios}}" id="repoVoluntarios"> --}}
                    <select class="form-control"  name="voluntario" id="voluntarios">
                        <option></option>
                        @foreach($voluntarios as $voluntario)
                            <option value="{{$voluntario->idvoluntario}}">{{ $voluntario->nombre .',  '.$voluntario->apellido . 'CTO: '. $voluntario->cto_nombre }}</option>                        
                        @endforeach
                    </select>
                    </div>
                    <span class="badge badge-danger">{{ $errors->first('voluntario')}}</span>
                </div>
                

                <div class="col-lg-6 col-sm-4">
                    <div class="form-group">
                    <label>Mesas:</label>
                    <input type="hidden" value="{{$mesas}}" id="repoMesas">
                    <select class="form-control"  name="mesas" id="mesas">
                        <option></option>
                        @foreach($mesas as $mesa)
                            <option value="{{$mesa->idmesa}}">{{$mesa->escuela_mesa.' - '. $mesa->numero_mesa  }}</option>                        
                        @endforeach
                    </select>
                    </div>
                    <span class="badge badge-danger">{{ $errors->first('mesa')}}</span>
                </div>

                </div>


                           <div>
                <button type="submit" class="btn btn-info d-block" style="width:100%">Guardar</button>
            </div>
        </form>
    </div>

@endsection


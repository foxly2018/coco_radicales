@extends('layout')
@php

    use App\Escuela;
    use App\CircuitoFiscal;
    use App\User;

@endphp
@section('title', 'Escuelas')

@section('seccionSaludo')
<h1>Listado de Escuelas</h1>
{{-- <h3 class="title text-center">---</h3> --}}
@endsection

@section('contenido')
    <div class="col-md-12">

    <div class="row">
        {{-- @can('fiscal-alta') --}}
                <a href="{{route('fiscal.alta')}}"; class="btn btn-success" style="margin-left: 89%;">Designar fiscal general</a>
                {{--    @endcan --}}
        </div>

        <div class="table-responsive" id="seccionTabla">
            <table class="table table-striped table-dark table-sm" id="tableVoluntarios" class="display compact" style="width:100%">
                <thead class="thead-dark">
                <tr>
                    <th>Circuito</th>
                    <th>Escuela</th>
                    <th>Fiscal General</th>
                    <th>Celular</th>
                    <th>Estado</th>
                    
                    <th></th>
                    <th></th>
                    
                </tr>
                </thead>
                <tbody>
                    @foreach($escuelas as $esc)  
                       
                       
                                
                        <tr>
                            <td> {{ $esc->circuito}}  </td>
                            @if (  $esc->estado == "completo")
                                 <td style="background-color:green">
                                    {{ $esc->nombre_escuela}}
                                </td>
                            @else
                                  <td >
                                    {{ $esc->nombre_escuela}}
                                </td>
                                @endif
                            <td>{{ $esc->nombre_fiscal . $esc->apellido_fiscal}}</td>
                            <td>{{ $esc->celular}}</td>
                            <td>{{ $esc->estado}}</td>
                            
                           
                            <td>
                                @can('voluntario-detalle')                                    
                                <a href="{{route('fiscales.show',$esc->idescuela)}}" class="showEscuela" id="show_{{ $esc->idescuela}}"><i class="far fa-eye fa-2x"></i></a>
                                                         
                                @endcan                          
                            </td>
                            <td>
                              <!--   @can('voluntario-edicion')                                    
                                <a href="{{route('voluntario.edit',$esc->idescuela)}}" class="editVoluntario" id="edit_{{ $esc->idescuela}}"><i class="far fa-edit fa-2x"></i></a>
                                @endcan -->
                         </tr>
                         
                    @endforeach
                </tbody>
            </table>

        </div>

        
    </div>

@endsection

@section('script')
<script>
/* EMERGENTES */
function mjsAlert(id){
    $(id).show('slow');
    setTimeout(function() {
    $('.message').hide('slow');
    }, 3500);
}// FIN EMERGENTES //
</script>
@endsection

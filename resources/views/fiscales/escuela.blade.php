@extends('layout')
@php
    
    use App\CircuitoFiscal;
    use App\Escuela;
    use App\Mesa;
    use App\Fiscal;
    use App\Voluntario;
    use App\Circuito;

@endphp
@section('title', 'Fiscales')

@section('seccionSaludo')
<h1>Listado de Fiscales</h1>
<h2>  {{ $escuela->idescuela .'- ' . $escuela->nombre_escuela}}  </h2>
{{-- <h3 class="title text-center">---</h3> --}}
@endsection

@section('contenido')
    <div class="col-md-12">

        <div class="row">
            {{-- @can('fiscal-alta') --}}
                
                <a href="{{route('fiscal.altaFiscal',$escuela->idescuela)}}" class="btn btn-success" id="altaFiscal_{{ $escuela->idescuela}}" style="margin-left: 89%;">Nuevo Fiscal</a>
              

                {{--    @endcan --}}
        </div>
    

        <div class="table-responsive" id="seccionTabla">
            <table class="table table-striped table-dark table-sm" id="tableFiscales" class="display compact" style="width:100%">
                <thead class="thead-dark">
                <tr>
                    <th>Mesa</th>    
                    <th>Nombre y Apellido</th>
                    <th>Celular</th>
                    <th>  </th>
                    
                    
                </tr>
                </thead>
                <tbody>
                    @foreach($fiscales as $f)  
                        <tr>
                           
                            <td>{{ $f->mesa}}</td>
                            <td>{{ $f->nombre . ', '. $f->apellido}}</td>
                            <td>{{ $f->celular}}</td>
                            <td>

                            @can('voluntario-eliminar')
                                <a href="javascript:;"
                                    onclick="eliminarFiscal({{$f->idfiscales}});"><i class="fas fa-times fa-2x"></i></a>
                                <form id="delete-form_{{ $f->idfiscales}}" action="{{ route('escuela.destroy',$f->idfiscales) }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                </form>                                     
                                @endcan

                                                      
                            </td>
                           
                            
                        </tr>
                    @endforeach
                </tbody>
            </table>

        </div>

        
        <div id="ajaxExito" class="alert alert-success message" style="display:none;bottom: 7%;text-align:center;width:80%;z-index: 1000;position: fixed;">

        </div>
        <div id="ajaxError" class="alert alert-danger message" style="display:none;bottom: 7%;text-align:center;width:80%;z-index: 1000;position: fixed;">
            
        </div>
    </div>

    <div class="row">
            <a href="{{route('fiscales.index')}}" class="btn btn-dark float-left" style="margin-bottom: 3%;">Volver</a>
        </div>
    @endsection
@extends('layout')

@section('title', 'Alta fiscal general')

@section('seccionSaludo')
<h1>Asignar Fiscal General</h1>
@endsection

@section('contenido')

    <div class="col-md-12">
        <div class="row">
            <a href="{{route('fiscales.index')}}" class="btn btn-dark float-left" style="margin-bottom: 3%;">Volver</a>
        </div>
    {{--         <div class="text-center">
            <h3 class="title">Nuevo Fiscal General</h3>
        </div> --}}
        <form id="fiscal_store" action="{{ route('fiscal.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            
           
            
            <div class="row">

                

                <div class="col-lg-6 col-sm-4">
                   <div class="form-group">
                     <label>Escuela:</label>
                     {{-- <input type="hidden" value="{{$escuelas}}" id="repoEscuela"> --}}
                     <select class="form-control"  name="escuela" id="escuelas">
                        <option></option>
                          @foreach($escuelas as $escuela)
                             <option value="{{$escuela->idescuela}}">{{$escuela->circuito_escuela .' - '. $escuela->nombre_escuela}}</option>                        
                          @endforeach
                     </select>
                  </div>
                     <span class="badge badge-danger">{{ $errors->first('escuela')}}</span>
                </div>

                <div class="col-lg-6 col-sm-4">
                    <div class="form-group">
                    <label>Dirigente:</label>
                    {{-- <input type="hidden" value="{{$dirigentes}}" id="repoDirigentes"> --}}
                    <select class="form-control"  name="dirigente" id="dirigentes">
                        <option></option>
                        @foreach($dirigentes as $dirigente)
                            <option value="{{$dirigente->id}}">{{$dirigente->nombre .',  ' .$dirigente->apellido}}</option>                        
                        @endforeach
                    </select>
                    </div>
                    <span class="badge badge-danger">{{ $errors->first('dirigente')}}</span>
                </div>
            </div>


                           <div>
                <button type="submit" class="btn btn-info d-block" style="width:100%">Guardar</button>
            </div>
        </form>
    </div>

@endsection


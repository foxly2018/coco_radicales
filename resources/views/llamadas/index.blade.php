@extends('layout')

@php
    use App\User;
    use App\Voluntario;
    use App\Circuito;
@endphp
@php
    use Spatie\Permission\Models\Role;
    use Spatie\Permission\Models\Permission;
@endphp
<!-- @section('title', 'Miembros') -->
@section('title', 'Llamadas')

@section('seccionSaludo')
<h1>Listado de Voluntarios llamados</h1>
{{-- <h3 class="title text-center">---</h3> --}}
@endsection

@section('contenido')
    <div class="col-md-12">
        <h3>Interacciones realizadas HOY {{$fechaHoy=date('d-m-Y')}} : <span class="badge badge-pill badge-success badge-pill" style="font-size: x-large;">{{$contador}}</span></h3>
        {{-- <h3>Sin llamadas</h3> --}}
        <div class="table-responsive" id="seccionTabla">
            <table class="table table-striped table-dark table-sm" id="tableLlamadas">
                <thead class="thead-dark">
                <tr>
                    <th>Nombre y Apellido</th>
                    <th>Fecha interaccion   </th>
                    <th>Indicadores</th>
                    <th>Celular/Telefono</th>
                    <th>Descripcion</th>
                    
                    <th>Usuario</th>
                   
                </tr>
                </thead>
                <tbody>
                    
                @foreach($voluntarios as $voluntario)  
                        <tr>

                            <td>{{ $voluntario->nombre .','.$voluntario->apellido }}</td>
                            <td>  {{ $voluntario->fecha}}</td>

                            
                            <th>
                                <span class="badge badge-pill badge-success">Hubo Intereaccion</span>
                                @switch($voluntario->calidad)
                                    @case(1)
                                        <span class="badge badge-pill badge-primary">Interaccion Positiva</span>                                        
                                        @break
                                    @case(2)                                        
                                        <span class="badge badge-pill badge-danger">Interaccion Negativa</span>                                    
                                        @break
                                    @default
                                        
                                @endswitch
                            </th>
                            <td>{{$voluntario->celular .'/'.$voluntario->telefono }}</td>
                            <td>{{ $voluntario->descripcion}}</td>
                            
                            
                            <td style="font-size=0.5em;"> <span style = "font-size : 0.9em ;"> {{ $voluntario->usernombre.' '.$voluntario->userapellido}}</span></td>
                        
                          
                        </tr>
                    @endforeach
                 
                </tbody>
                
            </table>
            
        </div>
    </div>


@endsection

@section('script')
<script>

$(document).ready(function() {
    setTimeout(function() {
      $('#message').fadeOut('fast');
    }, 3500);

    const TLlamadas = $('#tableLlamadas').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'pdfHtml5',
        ],
        "order": [],
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
        },
        // pageLength : 5,
        //lengthMenu: [[10, 20, -1], [10, 20, 'Todos']]
        
    });
    TLlamadas.page.len(20).draw();
});
</script>
@endsection

@extends('layout')
@php
    use App\Voluntario;
    use App\Circuito;

@endphp
@section('title', 'Voluntarios')

@section('seccionSaludo')
<h1>Listado de Voluntarios</h1>
{{-- <h3 class="title text-center">---</h3> --}}
@endsection

@section('contenido')
    <div class="col-md-12">

        <div class="row justify-content-center">
            <form action="{{route('voluntario.index')}}" method="get" class="col-8" id="circuitoFilter">
                <div class="card bg-Light text-dark col-12">
                    <div class="card-header">
                        <h5 class="card-title">Filtro de Circuito</h5>
                    </div>
                    <div class="card-body">                    
                            <input type="hidden" id="circuitos_seleccionados" name="circuitos_seleccionados">
                            <div class="form-group">
                                <label ></label>
                                <select class="form-control single" id="filtro_circuito">
                                    <option selected >Seleccione un circuito</option>
                                    @foreach ($circuitos as $circuito)
                                        <option value="{{$circuito->idcircuito}}" >{{$circuito->circuito_nombre}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label>circuitos seleccionados:</label>
                                        <input class="inputTag_circuito form-control" type="text" value="" data-role="tagsinput sometext" >
                                    </div>
                                </div>
                            </div>
                            
                        
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-success" id="btn_circuitoFilter">Buscar</button>
                    </div>
                </div>
            </form>
        </div>


        <div class="row">
            @can('voluntario-alta') 
                <a href="{{route('voluntario.alta')}}"; class="btn btn-success" style="margin-left: 89%;">Nuevo Voluntario</a>
            @endcan
        </div>

        <div class="table-responsive" id="seccionTabla">
            <table class="table table-striped table-dark table-sm" id="tableVoluntarios" class="display compact" style="width:100%">
                <thead class="thead-dark">
                <tr>
                    <th>Nombre y Apellido</th>
                    <th>Indicadores</th>
                    <th>Celular-DNI</th>
                    <th>Email</th>
                    <th>Circuito</th>
                    <th>Fecha de Alta</th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                    @foreach($voluntarios as $voluntario)  
                        <tr>
                            <td>
                                {{ $voluntario->apellido .','.$voluntario->nombre}}                                
                            </td>
                            <th>
                                @if ( $voluntario->confirmarInteraccion == 1)
                                    <span class="badge badge-pill badge-success">Hubo Intereaccion</span>
                                    @if ($voluntario->calidadInteraccion == 2)                                        
                                        <span class="badge badge-pill badge-danger">Re-Llamar</span>
                                    @else
                                        @php
                                            $fecha1 = date_create($voluntario->updated_at);
                                            $fecha2 = date_create();
                                            $dias = date_diff($fecha1, $fecha2)->format('%R%a');
                                        @endphp
                                        @if ($dias >= 14 )
                                            <span class="badge badge-pill badge-danger">Re-Llamar</span>
                                        @endif
                                    @endif
                                @else
                                    @if ($voluntario->confirmarInteraccion != null && $voluntario->confirmarInteraccion == 2)
                                        <span class="badge badge-pill badge-warning">No hubo Interaccion</span>
                                        <span class="badge badge-pill badge-danger">Re-Llamar</span>
                                    @endif
                                @endif
                                @if ($voluntario->cantNegativas > 0)
                                    <span class="badge badge-pill badge-primary">Cant. de Negativas: {{$voluntario->cantNegativas}}</span>
                                @endif
                            </th>
                            <td style="text-align: start;">
                                Cel: {{ $voluntario->celular}}<br>
                                Direccion: {{ $voluntario->dir}}
                            </td>
                            <td>{{ $voluntario->email}}</td>
                            <td>{{ Circuito::find($voluntario->circuito_idcircuito)->circuito_nombre}}</td>
                            <td>{{ $voluntario->created_at}}</td>
                            <td>
                                @can('voluntario-detalle')                                    
                                <a href="javascript:;" class="showVoluntario" id="show_{{ $voluntario->idvoluntario}}"><i class="far fa-eye fa-2x"></i></a>                            
                                @endcan                          
                            </td>
                            <td>
                                @can('voluntario-edicion')                                    
                                <a href="{{route('voluntario.edit',$voluntario->idvoluntario)}}" class="editVoluntario" id="edit_{{ $voluntario->idvoluntario}}"><i class="far fa-edit fa-2x"></i></a>
                                @endcan
                            </td>
                            <td>
                                @can('voluntario-eliminar')
                                <a href="javascript:;"
                                    onclick="eliminarVoluntario({{ $voluntario->idvoluntario}});"><i class="fas fa-times fa-2x"></i></a>
                                <form id="delete-form_{{ $voluntario->idvoluntario}}" action="{{ route('voluntario.destroy',$voluntario->idvoluntario) }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                </form>                                     
                                @endcan
                                
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

        </div>

        <div class="text-center" id="seccionDatos" style="display:none;">
            <h3 id="nombreVoluntario"></h3>
            <div class="row">
                
                @include('view_repositorios.card_info', array('cardHeader'=>'CONTACTO'))
                
                <div class="card bg-Light text-dark col-12">
                    <div class="card-header">INTERACCIONES</div>
                    <div class="card-body">
                        <div class="row" style="justify-content: flex-end;">
                            <form action="{{ route('btn_imprimir', ['id'=>1]) }}" method="get" id="fromImprimirInteracciones">
                                <input type="hidden" name="id_volutario_imprimir" id="id_volutario_imprimir">
                            </form>
                            <button class="btn btn-info" id="btn_imprimirInteracciones">Imprimir</button>
                            @can('voluntario-alta-interaccion')                                
                                <button class="btn btn-primary" id="btnNewInteraccion"
                                data-toggle="modal" 
                                data-target="#ModalInteraccion">Nueva Interaccion<div class="ripple-container"></div></button>
                            @endcan

                        </div>
                        
                        <div class="table-responsive">
                            <table class="table table-striped" id="tablaInteracciones">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Fecha</th>
                                        <th>Tipo</th>
                                        <th>Descripcion</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
                
            </div>
            <button class="btn btn-primary btn-sm pull-left" id="volverList">Volver al listado<div class="ripple-container"></div></button>
        </div>
        <div id="ajaxExito" class="alert alert-success message" style="display:none;bottom: 7%;text-align:center;width:80%;z-index: 1000;position: fixed;">

        </div>
        <div id="ajaxError" class="alert alert-danger message" style="display:none;bottom: 7%;text-align:center;width:80%;z-index: 1000;position: fixed;">
            
        </div>
    </div>

@endsection

@section('script')
<script>
var tableInteracciones;
$(document).ready(function() {
    setTimeout(function() {
      $('#message').fadeOut('fast');
    }, 3500);

    const TVoluntarios = $('#tableVoluntarios').DataTable({
         dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'pdfHtml5',
        ], 
        "order": [],
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
        },
        // pageLength : 5,
        //lengthMenu: [[10, 20, -1], [10, 20, 'Todos']],
        
    });
    TVoluntarios.page.len(20).draw();
    tableInteracciones = $('#tablaInteracciones').DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
        },
        // pageLength : 5,
        lengthMenu: [[5,10, 20, -1], [5,10, 20, 'Todos']],
        
    });
} );
/* SHOW DATOS DEL VOLUNTARIO E INTERACCIONES */
$(document).on('click','.showVoluntario',function(){
    let id = $(this).prop('id').split('_')[1];
    let ruta  =  "{{ URL::to('voluntario_show/') }}"+"/"+id;
    $.ajax({
      type: 'GET',
      url: ruta,
      //data: form.serialize(),
      beforeSend: function(){ },
      error: function(jqxhr, textStatus, error){
          console.log(error);
      },
      success: function(respuesta){
        
        if (respuesta.status ) {
            tableInteracciones.clear().draw();
           // console.log(respuesta.interacciones[0].pivot);
            respuesta.interacciones.forEach((element,index) => {
                tableInteracciones.row.add( [
                    index+1,
                    element.pivot.fecha,
                    respuesta.tipoInteraccion.filter(TI => TI.id == element.pivot.tipo_interaccion_id)[0].tipo_nombre,
                    element.pivot.descripcion
                ] ).draw( false );
            });
            
            

            $('#nombreVoluntario').html(respuesta.voluntario.apellido+', '+respuesta.voluntario.nombre);
            $('#foto').attr( "src" ,respuesta.voluntario.foto);
            $('#direccion').html(respuesta.direccion.calle+' '+respuesta.direccion.numero);
            $('#telefono').html(respuesta.voluntario.telefono);
            $('#celular').html(respuesta.voluntario.celular);
            $('#origen').html(respuesta.voluntario.origen);
            $('#correo').html(respuesta.voluntario.email);
            $('#localidad').html(respuesta.localidad.localidad_nombre);
            $('#departamento').html(respuesta.departamento.departamento_nombre);
            $('#interaccionTipo').html('');
            respuesta.tipoInteraccion.forEach(elem => {
                $('#interaccionTipo').append('<option value="'+elem.id+'">'+elem.tipo_nombre+'</option>');
            });
            $('#voluntarios_idvoluntario').val(respuesta.voluntario.idvoluntario);
            $('#id_volutario_imprimir').val(respuesta.voluntario.idvoluntario);

            $('#seccionTabla').hide('slow');
            $('#seccionDatos').show('slow');
        }
      },
      dataType: 'json',
      async:true
    });

});
$(document).on('click','#InteraccionCerrar',function () {
    $('#fromAltaInterccion')[0].reset();
    $('#ModalInteraccion').modal('hide');
});
$(document).on('change','#confirmarInteraccion1, #confirmarInteraccion2',function(e){
    if ($(this).val() == 2) {
    $('#interaccionTipo').prop('disabled',true)
    $('#interaccionDescriptcion').prop('disabled',true)
    }else{
        $('#interaccionTipo').prop('disabled',false)
        $('#interaccionDescriptcion').prop('disabled',false)
    }
});
/* ALTA DE INTERACCION */
$(document).on('click','#InteraccionAceptar',function(){
    let form = $('#fromAltaInterccion');
    $.ajax({
      type: 'POST',
      url: "{{ route('interaccion.store') }}",
      data: form.serialize(),
      beforeSend: function(){ },
      error: function(jqxhr, textStatus, error){
          console.log(error);
          console.log(jqxhr);
          let id = '#ajaxError';
          $(id).html(jqxhr.responseJSON);
          mjsAlert(id);
      },
      success: function(respuesta){
        if (respuesta.status ) {
            form[0].reset();
            $('#ModalInteraccion').modal('hide');

            console.log(respuesta.interacciones);
            tableInteracciones.clear().draw();
            respuesta.interacciones.forEach((element,index) => {
                tableInteracciones.row.add( [
                    index+1,
                    element.pivot.fecha,
                    respuesta.tipoInteraccion.filter(TI => TI.id == element.pivot.tipo_interaccion_id)[0].tipo_nombre,
                    element.pivot.descripcion
                ] ).draw( false );
            });
            let id = '#ajaxExito';
            $(id).html(respuesta.mjs);
            mjsAlert(id);
        }
      },
      dataType: 'json',
      async:true
    });

});
$(document).on('click','#volverList',function(){
    $('#seccionDatos').hide('slow');
    $('#seccionTabla').show('slow');
});
$(document).on('click','#btn_imprimirInteracciones',function () {
    $('#fromImprimirInteracciones').submit();
});
function eliminarVoluntario(idvoluntario){
    if(confirm('Esta acción no podrá deshacerse. ¿Continuar?')){

        document.getElementById('delete-form_'+idvoluntario).submit();
    }
}
/* EMERGENTES */
function mjsAlert(id){
    $(id).show('slow');
    setTimeout(function() {
    $('.message').hide('slow');
    }, 3500);
}// FIN EMERGENTES //

$('.single').select2();
    $('.select2-container').on('click',function (a) {
      $("input[type=search]").attr("placeholder", "Buscar...");
});
$(document).on('click','#btn_circuitoFilter',function(){
    event.preventDefault();
    var tags_circuitos = $('.inputTag_circuito').tagsinput('items');
        $('#circuitos_seleccionados').val(JSON.stringify(tags_circuitos));

    console.log(JSON.stringify(tags_circuitos));
   document.getElementById('circuitoFilter').submit();

});
$(document).ready(function(){
    $('.inputTag_circuito').tagsinput({// CONFIGURACION
        allowDuplicates: false,
        tagClass: 'badge badge-primary',
        itemValue: 'id',
        itemText: 'text'
    });
});
$('#filtro_circuito').on('change',function(){ //CONTROL PARA AGREGAR TAGS DE CIRCUITOS
    if($(this).val()!=""){

        let obj = {
            id:$(this).val(),
            text:$('#filtro_circuito option:selected').html()
        };
        $('.inputTag_circuito').tagsinput('add', obj);

    } 
});







</script>
@endsection

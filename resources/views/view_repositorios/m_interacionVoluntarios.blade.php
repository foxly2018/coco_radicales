<div class="modal fade" id="ModalInteraccion" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title"><strong> Cargar Interacción</strong></h4>
            {{-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <i class="material-icons">clear</i>
            </button> --}}
          </div>
          <div class="modal-body">

            <form action="{{ route('interaccion.store') }}" method="post" id="fromAltaInterccion">
              @csrf
              <input type="hidden" name="voluntarios_idvoluntario" id="voluntarios_idvoluntario" value="">

              <div class="row justify-content-around">
                <div class="form-check">
                  <label class="form-check-label">
                    <input class="form-check-input" type="radio" name="confirmarInteraccion" id="confirmarInteraccion1" value="1" checked>
                    Hubo Interaccion
                    <span class="circle">
                      <span class="check"></span>
                    </span>
                  </label>
                </div>
                <div class="form-check">
                  <label class="form-check-label">
                    <input class="form-check-input" type="radio" name="confirmarInteraccion" id="confirmarInteraccion2" value="2">
                    No hubo Interaccion
                    <span class="circle">
                      <span class="check"></span>
                    </span>
                  </label>
                </div>
              </div>
              <div class="form-group">
                <label for="sel1">Tipo de Contacto:</label>
                <select class="form-control" name="interaccionTipo" id="interaccionTipo">
                  
                </select>
              </div>
              <div class="form-group">
                <label for="comment">Descripción:</label>
                <textarea class="form-control" rows="5" name="interaccionDescriptcion" id="interaccionDescriptcion"></textarea>
              </div>
              <h5 class="text-center">La interacción fue:</h5>
              <div class="row justify-content-around">
                <div class="form-check">
                  <label class="form-check-label">
                    <input class="form-check-input" type="radio" name="calidadInteraccion" id="calidadInteraccion1" value="1"  checked>
                    Positiva
                    <span class="circle">
                      <span class="check"></span>
                    </span>
                  </label>
                </div>
                <div class="form-check">
                  <label class="form-check-label">
                    <input class="form-check-input" type="radio" name="calidadInteraccion" id="calidadInteraccion2" value="2">
                    Negativa
                    <span class="circle">
                      <span class="check"></span>
                    </span>
                  </label>
                </div>
              </div>
              <h5 class="text-center">Quiere ser fiscal:</h5>
              <div class="row justify-content-around">
                <div class="form-check">
                  <label class="form-check-label">
                    <input class="form-check-input" type="radio" name="serFiscal" id="serFiscal1" value="1" >
                    SI quiere ser fiscal
                    <span class="circle">
                      <span class="check"></span>
                    </span>
                  </label>
                </div>
                <div class="form-check">
                  <label class="form-check-label">
                    <input class="form-check-input" type="radio" name="serFiscal" id="serFiscal2" value="2">
                    NO quiere
                    <span class="circle">
                      <span class="check"></span>
                    </span>
                  </label>
                </div>
              </div>


            </form>

          </div>
          <div class="modal-footer">
          <button type="button" class="btn btn-danger btn-link"{{--  data-dismiss="modal" --}} id="InteraccionCerrar">Cerrar</button>
          <button type="button" class="btn btn-link" id="InteraccionAceptar">Guardar</button>
          </div>
      </div>
    </div>
</div>
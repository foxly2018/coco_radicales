

@extends('layout')
@php
    use Spatie\Permission\Models\Role;
    use Spatie\Permission\Models\Permission;
@endphp

@section('title', 'usuarios')

@section('seccionSaludo')
<h1>Listado de usuarios</h1>
{{-- <h3 class="title text-center">---</h3> --}}
@endsection

@section('contenido')

    <div class="col-md-12">
        <div class="row">
            @can('usuario-alta')                
            <a href="{{route('usuarios.create')}}"; class="btn btn-success" style="margin-left: 89%;">Nuevo</a>
            @endcan
        </div>
        <div class="table-responsive" id="seccionTabla">
            <table class="table table-striped table-dark table-sm" id="tableUsers">
                <thead class="thead-dark">
                <tr>
                    <th>Nombre y Apellido</th>
                    <th>Celular</th>
                    <th>Email</th>
                    <th>Rol</th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)  
                        <tr>
                            <td>{{ $user->apellido .','.$user->nombre}}</td>
                            <td>{{ $user->celular}}</td>
                            <td>{{ $user->email}}</td>                              
                            <td>
                                @if (count($user->getRoleNames())>0)
                                    @if ($user->getRoleNames()[0] == 'Miembro')
                                    Coordinador                              
                                    @else                                
                                    {{ $user->getRoleNames()[0]}}
                                    @endif   
                                @else
                                <span class="badge badge-pill badge-danger">Sin Rol</span>
                                @endif
                             
                            </td>
                            <td>
                                @can('usuario-detalle')                
                                <a href="javascript:;" class="showuser" id="show_{{ $user->id}}"><i class="far fa-eye fa-2x"></i></a>                               
                                @endcan
                            </td>
                            <td>
                                @can('usuario-edicion')                
                                <a href="{{route('usuarios.edit',$user->id)}}" class="edituser" id="edit_{{ $user->id}}"><i class="far fa-edit fa-2x"></i></a>
                                @endcan
                            </td>
                            <td>
                                @can('usuario-eliminar')                
                                <a href="javascript:;"
                                onclick="eliminarUser({{ $user->id}});"><i class="fas fa-times fa-2x"></i></a>
                                <form id="delete-form_{{ $user->id}}" action="{{ route('usuarios.destroy',$user->id) }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                </form> 
                                @endcan
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

        </div>

        <div class="text-center" id="seccionDatos" style="display:none;">
            <h3 id="nombreUsuario"></h3>
            <div class="row">
                @include('view_repositorios.card_info', array('cardHeader'=>'USUARIO'))                
            </div>
            <div class="title">
                <h3><small>RESPONSABILIDADES</small></h3>
            </div>
            <div class="row">

                <div class="col-lg-12 col-md-12">
                  <div class="row">
                    <div class="col-md-3">
                      <ul class="nav nav-pills nav-pills-primary nav-pills-icons flex-column" role="tablist">
                        <!--
                                            color-classes: "nav-pills-primary", "nav-pills-info", "nav-pills-success", "nav-pills-warning","nav-pills-danger"
                                        -->
                        <li class="nav-item">
                          <a class="nav-link active" href="#sec-circuito" role="tab" data-toggle="tab">
                            <i class="fas fa-road fas-x2"></i>
                            Circuitos
                          </a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="#sec-localidades" role="tab" data-toggle="tab">
                            <i class="fas fa-map-marked-alt fas-x2"></i>
                            Localidades
                          </a>
                        </li>
                      </ul>
                    </div>
                    <div class="col-md-8">
                      <div class="tab-content">
                        <div class="tab-pane active" id="sec-circuito">                            
                                {{-- <span class="badge badge-pill badge-info">Info</span>     --}}                        
                        </div>
                        <div class="tab-pane" id="sec-localidades">
                         
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
            <button class="btn btn-primary btn-sm pull-left" id="volverList">Volver al listado<div class="ripple-container"></div></button>
        </div>
    </div>


@endsection

@section('script')
<script>

$(document).ready(function() {
    setTimeout(function() {
      $('#message').fadeOut('fast');
    }, 3500);

    const TUsuarios = $('#tableUsers').DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
        },
        // pageLength : 5,
        lengthMenu: [[10, 20, -1], [10, 20, 'Todos']]
        
    });
    TUsuarios.page.len(20).draw();
});
function eliminarUser(idUser){
    if(confirm('Esta acción no podrá deshacerse. ¿Continuar?')){

        document.getElementById('delete-form_'+idUser).submit();
    }
}
$(document).on('click','.showuser',function(){
    let id = $(this).prop('id').split('_')[1];
    let ruta  =  "{{ URL::to('usuarios/') }}"+"/"+id;
    $.ajax({
      type: 'GET',
      url: ruta,
      //data: form.serialize(),
      beforeSend: function(){ },
      error: function(jqxhr, textStatus, error){
          console.log(error);
          /* let a = ( jqxhr.responseJSON.errors.palabra != null )?jqxhr.responseJSON.errors.palabra:'';
            $('.palabra').html(a);

          console.log(jqxhr);
          let id = '#ajaxError';
          $(id).html('ocurio un error vuelva a intentar');
          mjsAlert(id);*/
      },
      success: function(respuesta){
        
        if (respuesta.status ) {
            $('#nombreUsuario').html(respuesta.usuario.apellido+', '+respuesta.usuario.nombre);
            $('#foto').attr( "src" ,respuesta.usuario.foto);
            $('#direccion').html(respuesta.direccion.calle+' '+respuesta.direccion.numero);
            $('#telefono').html(respuesta.usuario.telefono);
            $('#celular').html(respuesta.usuario.celular);
            $('#correo').html(respuesta.usuario.email);
            $('#localidad').html(respuesta.localidad.localidad_nombre);
            $('#departamento').html(respuesta.departamento.departamento_nombre);
            respuesta.respCircuitos.forEach(element => {
                let span = `<span class="badge badge-pill badge-info">${element.circuito_nombre}</span>`;
                $('#sec-circuito').append(span);
            });
            respuesta.respLocalidades.forEach(element => {
                let span = `<span class="badge badge-pill badge-info">${element.localidad_nombre}</span>`;
                $('#sec-localidades').append(span);
            });
            $('#seccionTabla').hide('slow');
            $('#seccionDatos').show('slow');
        }
      },
      dataType: 'json',
      async:true
    });

});

$(document).on('click','#volverList',function(){
    $('#seccionDatos').hide('slow');
    $('#seccionTabla').show('slow');
});
</script>
@endsection

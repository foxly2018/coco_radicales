@extends('layout')

@section('title', 'Alta Usuarios')

@section('seccionSaludo')
<h1>Nuevo Usuario</h1>
@endsection

@section('contenido')

    <div class="col-md-12">
        <div class="row">
            <a href="{{route('usuarios.index')}}" class="btn btn-dark float-left" style="margin-bottom: 3%;">Volver</a>
        </div>
{{--         <div class="text-center">
            <h3 class="title">Nuevo usuarios</h3>
        </div> --}}
        <form id="usuarios_store" action="{{ route('usuarios.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-lg-6 col-sm-4">
                    <div class="form-group has-default">
                    <label>Nombre:</label>
                    <input type="text" class="form-control" name="nombre" 
                    value="{{old('nombre')}}">
                    </div>
                    
                    <span class="badge badge-danger">{{ $errors->first('nombre')}}</span>
                </div>
                <div class="col-lg-6 col-sm-4">
                    <div class="form-group has-default">
                    <label>Apellido:</label>
                    <input type="text" class="form-control" name="apellido" 
                    value="{{old('apellido')}}">
                    </div>
                    <span class="badge badge-danger">{{ $errors->first('nombre')}}</span>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-sm-4">
                    <div class="form-group has-default">
                    <label>DNI:</label>
                    <input type="number" class="form-control" min="1" step="1" name="dni" 
                    value="{{old('dni')}}">
                    </div>
                    <span class="badge badge-danger">{{ $errors->first('dni')}}</span>
                </div>
                <div class="col-lg-6 col-sm-4">
                    <div class="form-group has-default">
                    <label>Correo:</label>
                    <input type="email" class="form-control" name="email" 
                    value="{{old('email')}}">
                    </div>
                    <span class="badge badge-danger">{{ $errors->first('email')}}</span>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-sm-4">
                    <div class="form-group has-default">
                    <label>Celular:</label>
                    <input type="number" class="form-control" min="1" step="1" name="celular" 
                    value="{{old('celular')}}">
                    </div>
                    <span class="badge badge-danger">{{ $errors->first('celular')}}</span>
                </div>
                <div class="col-lg-6 col-sm-4">
                    <div class="form-group has-default">
                    <label>Telefono:</label>
                    <input type="number" class="form-control" name="telefono">
                    </div>
                    
                </div>
            </div>
            <div class="row">

                <div class="col-lg-6 col-sm-4">
                    <div class="form-group">
                    <label>Departamentos:</label>
                    <select class="form-control"  name="departamento" id="departamentos">
                        <option></option>
                        @foreach($departamentos as $depto)
                            <option value="{{$depto->iddepartamento}}">{{$depto->departamento_nombre}}</option>                        
                        @endforeach
                    </select>
                    </div>
                    <span class="badge badge-danger">{{ $errors->first('departamento')}}</span>
                </div>
                <div class="col-lg-6 col-sm-4">
                    <div class="form-group">
                    <label>Localidades:</label>
                    <input type="hidden" value="{{$localidades}}" id="repoLocalidades">
                    <select class="form-control"  name="localidad" id="localidades">
                        <option></option>
                        @foreach($localidades as $localidad)
                            <option value="{{$localidad->idlocalidad}}">{{$localidad->localidad_nombre}}</option>                        
                        @endforeach
                    </select>
                    </div>
                    <span class="badge badge-danger">{{ $errors->first('localidad')}}</span>
                </div>
                
            </div>
            <div class="row">
                <div class="col-lg-6 col-sm-4">
                    <div class="form-group has-default">
                    <label>Fecha de Nacimiento:</label>
                    <input type="date" class="form-control" name="fecha_nacimiento" 
                    value="{{old('fecha_nacimiento')}}">
                    </div>
                    <span class="badge badge-danger">{{ $errors->first('fecha_nacimiento')}}</span>
                </div>
                <div class="col-lg-6 col-sm-4">
                    <div class="form-group">
                    <label>Circuitos:</label>
                    <input type="hidden" value="{{$circuitos}}" id="repoCircuitos">
                    <select class="form-control"  name="circuitos" id="circuitos">
                        <option></option>
                        @foreach($circuitos as $circuito)
                            <option value="{{$circuito->idcircuito}}">{{$circuito->circuito_nombre}}</option>                        
                        @endforeach
                    </select>
                    </div>
                    <span class="badge badge-danger">{{ $errors->first('circuitos')}}</span>
                </div>
            </div>

                <h5 for="sel1">Direccion:</h5>
            <div class="row">
                <div class="col-lg-6 col-sm-4">
                    <div class="form-group has-default">
                    <label>Calle:</label>
                    <input type="text" class="form-control" name="calle" 
                    value="{{old('calle')}}">
                    </div>
                    <span class="badge badge-danger">{{ $errors->first('calle')}}</span>
                </div>
                <div class="col-lg-6 col-sm-4">
                    <div class="form-group has-default">
                    <label>Número:</label>
                    <input type="number" class="form-control" name="numero" 
                    value="{{old('numero')}}">
                    </div>
                    <span class="badge badge-danger">{{ $errors->first('numero')}}</span>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-sm-4">
                    <div class="form-group has-default">
                    <label>Piso:</label>
                    <input type="text" class="form-control" name="piso">
                    </div>
                </div>
                <div class="col-lg-6 col-sm-4">
                    <div class="form-group has-default">
                    <label>Departamento:</label>
                    <input type="text" class="form-control" name="dpto" >
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-sm-4">
                    <div class="form-group has-default">
                    <label>Manzana:</label>
                    <input type="text" class="form-control" name="manzana">
                    </div>
                </div>
                <div class="col-lg-6 col-sm-4">
                    <div class="form-group has-default">
                    <label>Casa:</label>
                    <input type="text" class="form-control" name="casa">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-sm-4">
                    <div class="form-group has-default">
                        <label>Contraseña:</label>
                        <input type="password" 
                                class="form-control" 
                                name="password">
                    </div>
                </div>
                <div class="col-lg-6 col-sm-4">
                    <div class="form-group">
                    <label>Roles:</label>
                    <select class="form-control"  name="roles" id="roles">
                        <option></option>
                        @foreach($roles as $rol)
                            <option value="{{$rol->name}}">
                                @if ($rol->id == 2)
                                Coordinador                              
                                @else                                
                                {{ $rol->name}}
                                @endif
                            </option>                        
                        @endforeach
                    </select>
                    </div>
                    <span class="badge badge-danger">{{ $errors->first('roles')}}</span>
                </div> 
            </div>

            <h5>Seleccione Responsabilidades:</h5>
            <div class="row">

                <div class="col-lg-6 col-sm-4">
                    <input type="hidden" id="resp_circuitos" name="resp_circuitos">
                    <div class="form-group">
                        <label>Por circuitos:</label>
                        <select class="form-control single" name="select_resp_circuitos" id="select_resp_circuitos">
                            <option></option>
                            @foreach($circuitos as $circuito)
                                <option value="{{$circuito->idcircuito}}">{{$circuito->circuito_nombre}}</option>                        
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-4">
                    <input type="hidden" id="resp_localidades" name="resp_localidades">
                    <div class="form-group">
                        <label>Por Localidad:</label>
                        <select class="form-control single" name="select_resp_localidades" id="select_resp_localidades">
                            <option></option>
                            @foreach($localidades as $localidad)
                                <option value="{{$localidad->idlocalidad}}">{{$localidad->localidad_nombre}}</option>                        
                            @endforeach
                        </select>
                    </div>
                </div>
                
            </div>
            <div class="row">{{-- TAGS --}}

                <div class="col-lg-6 col-sm-4">
                    <div class="form-group">
                        <label>circuitos seleccionados:</label>
                        <input class="inputTag_circuito form-control" type="text" value="" data-role="tagsinput sometext" >
                    </div>
                </div>
                <div class="col-lg-6 col-sm-4">
                    <div class="form-group">
                        <label>localidades seleccionadas:</label>
                        <input class="inputTag_localidad form-control" type="text" value="" data-role="tagsinput sometext" >
                    </div>
                </div>                
            </div>

            <div class="row d-flex justify-content-center" style="margin-bottom: 4%;">
                <div class="d-flex justify-content-around">
                    <img class="img-raised rounded img-fluid" id="Foto" src="" alt="Foto" style="width: 35%;height: 35vh;display:none;">
                    <div class="">
                        <input type="file" class="form-control" id="fotousuario" name="file">
                        <span class="badge badge-danger">{{ $errors->first('file')}}</span>
                    </div>
                </div>
                               
            </div>
            <div>
                <button id="guardarUser" class="btn btn-info d-block" style="width:100%">Guardar</button>
            </div>
        </form>
    </div>

@endsection

@section('script')
<script>

$(document).ready(function(){
    var localidades = JSON.parse( $('#repoLocalidades').val() );
    var circuitos = JSON.parse($('#repoCircuitos').val());
    $('#departamentos').change(function(e){
        var iddpto = $(this).val();
        $('#localidades').html('');
        $('#localidades').append('<option></option>');
        localidades.forEach((elem)=>{
            if(iddpto == elem.departamento_iddepartamento){
                $('#localidades').append('<option value="'+elem.idlocalidad+'">'+elem.localidad_nombre+'</option>');
            }
        });
        $('#circuitos').html('');
    });
    $('#localidades').change(function(e){
        var idlocalidad = $(this).val();
        $('#circuitos').html('');
        $('#circuitos').append('<option></option>');
        circuitos.forEach((elem)=>{
            if(idlocalidad == elem.localidad_idlocalidad){
                $('#circuitos').append('<option value="'+elem.idcircuito+'">'+elem.circuito_nombre+'</option>');
            }
        });
    });
    $("#fotousuario").on("change", function(e) {
        var TmpPath = URL.createObjectURL(e.target.files[0]);
        // Mostramos la ruta temporal
        $('#Foto').attr('src', TmpPath);
        $('#Foto').show();
    });
});
/* -------------parte para los tags y envio del form --------------*/
$('.single').select2();
    $('.select2-container').on('click',function (a) {
      $("input[type=search]").attr("placeholder", "Buscar...");
    });

$(document).on('click','#guardarUser',function(){
    event.preventDefault();
    var tags_circuitos = $('.inputTag_circuito').tagsinput('items');
        $('#resp_circuitos').val(JSON.stringify(tags_circuitos));
    
    var tags_localidades = $('.inputTag_localidad').tagsinput('items');
    $('#resp_localidades').val(JSON.stringify(tags_localidades));

    console.log(JSON.stringify(tags_circuitos));
   document.getElementById('usuarios_store').submit();

});
$(document).ready(function(){
    $('.inputTag_circuito').tagsinput({// CONFIGURACION
        allowDuplicates: false,
        tagClass: 'badge badge-primary',
        itemValue: 'id',
        itemText: 'text'
    });
    $('.inputTag_localidad').tagsinput({// CONFIGURACION
        allowDuplicates: false,
        tagClass: 'badge badge-primary',
        itemValue: 'id',
        itemText: 'text'
    });
});
$('#select_resp_circuitos').on('change',function(){ //CONTROL PARA AGREGAR TAGS DE CIRCUITOS
    if($(this).val()!=""){

        let obj = {
            id:$(this).val(),
            text:$('#select_resp_circuitos option:selected').html()
        };
        $('.inputTag_circuito').tagsinput('add', obj);

    } 
});
$('#select_resp_localidades').on('change',function(){ //CONTROL PARA AGREGAR TAGS DE LOCALIDADES
    if($(this).val()!=""){

        let obj = {
            id:$(this).val(),
            text:$('#select_resp_localidades option:selected').html()
        };
        $('.inputTag_localidad').tagsinput('add', obj);

    } 
});
</script>
@endsection

<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    Login
  </title>
  <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href= "{{asset('static/css/material-kit.css?v=2.0.7')}}" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="{{asset('static/demo/demo.css')}}" rel="stylesheet" />
</head>

<body class="login-page sidebar-collapse">

  <div class="page-header header-filter" style="background-image: url('{{asset('static/img/azul_puntos.jpg')}}'); background-size: cover; background-position: top center; height: 100vh !important;">
    <div class="container">
      <div class="row" style="padding-top: 10%;">
        <div class="col-lg-4 col-md-6 ml-auto mr-auto">
          <div class="card card-login">
            <form class="form" method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}
              <div class="card-header card-header-primary text-center">
                <h4 class="card-title">Login</h4>
{{--                 <div class="social-line">
                  <a href="#pablo" class="btn btn-just-icon btn-link">
                    <i class="fa fa-facebook-square"></i>
                  </a>
                  <a href="#pablo" class="btn btn-just-icon btn-link">
                    <i class="fa fa-twitter"></i>
                  </a>
                  <a href="#pablo" class="btn btn-just-icon btn-link">
                    <i class="fa fa-google-plus"></i>
                  </a>
                </div> --}}
                @if(session()->has('flash'))
                    <div class="alert alert-info" style="text-align: center;">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{session('flash')}}
                    </div>
                @endif
              </div>
              {{-- <p class="description text-center">o ser clasico</p> --}}
              <div class="card-body" style="padding-top: 15%;">
{{--                 <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="material-icons">face</i>
                    </span>
                  </div>
                  <input type="text" class="form-control" placeholder="Nombre...">
                </div> --}}
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="material-icons">mail</i>
                    </span>
                  </div>
                  <input type="email"
                          class="form-control" 
                          name="email" 
                          placeholder="Email..."
                          value="{{old('email')}}">
                </div>
                  <span class="badge badge-danger">{{ $errors->first('email')}}</span>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="material-icons">lock_outline</i>
                    </span>
                  </div>
                  <input type="password" 
                          class="form-control" 
                          name="password" 
                          placeholder="Contraseña...">
                </div>
                  <span class="badge badge-danger">{{ $errors->first('password')}}</span>
              </div>
              <div class="footer text-center">
                    {{-- <div class="col-lg-12 text-center">
                        <button type="submit" class="btn btn-block btn-dark">Entrar</button>
                    </div> --}}
                <a href="#" class="btn btn-primary btn-link btn-wd btn-lg" id="entrar">Entrar</a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <footer class="footer">
      <div class="container">
      </div>
    </footer>
  </div>
  <!--   Core JS Files   -->
  <script src="{{asset('static/js/core/jquery.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('static/js/core/popper.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('static/js/core/bootstrap-material-design.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('static/js/plugins/moment.min.js')}}"></script>
  <!--	Plugin for the Datepicker, full documentation here: https://github.com/Eonasdan/bootstrap-datetimepicker -->
  <script src="{{asset('static/js/plugins/bootstrap-datetimepicker.js')}}" type="text/javascript"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="{{asset('static/js/plugins/nouislider.min.js')}}" type="text/javascript"></script>
  <!--  Google Maps Plugin    -->
  <!-- Control Center for Material Kit: parallax effects, scripts for the example pages etc -->
  <script src="{{asset('static/js/material-kit.js?v=2.0.7')}}" type="text/javascript"></script>

  <script>
    $('#entrar').click(function(){
        $('.form').submit();
    });
  </script>
</body>

</html>
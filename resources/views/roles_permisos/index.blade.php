@extends('layout')

@section('title', 'Roles')

@section('seccionSaludo')
<h1>Listado de Roles y Permisos</h1>
@endsection

@section('contenido')

    <div class="col-md-12">

        <div class="row">
            @can('rol-alta')                
            <a href="{{route('roles-permisos.create')}}"; class="btn btn-success" style="margin-left: 89%;">Nuevo Rol - Permisos</a>
            @endcan
        </div>
        <div class="table-responsive" id="seccionTabla">
            <table class="table table-striped table-dark table-sm" id="tableRolPermisos">
                <thead class="thead-dark">
                <tr>
                    <th>Rol</th>
                    <th>CRUD</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($roles as $rol)  
                        <tr>
                            @if ($rol->id == 2)
                            <td>Coordinador</td>                                
                            @else                                
                            <td>{{ $rol->name}}</td>
                            @endif
                            <td>
                                @can('rol-edicion')                
                                <a href="{{route('roles-permisos.edit',$rol->id)}}" class="editrol" id="edit_{{ $rol->id}}"><i class="far fa-edit fa-2x"></i></a>
                                @endcan
                                @can('rol-eliminar')                
                                <a href="javascript:;"
                                    onclick="eliminarrol({{ $rol->id}});"><i class="fas fa-times fa-2x"></i></a>
                                <form id="delete-form_{{ $rol->id}}" action="{{ route('roles-permisos.destroy',$rol->id) }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                </form>                                
                                @endcan
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

        </div>

    </div>

@endsection

@section('script')
<script>

$(document).ready(function() {
    setTimeout(function() {
      $('#message').fadeOut('fast');
    }, 3500);

    const TRolPermisos = $('#tableRolPermisos').DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
        },
        // pageLength : 5,
        lengthMenu: [[10, 20, -1], [10, 20, 'Todos']]
        
    });
    TRolPermisos.len(20).draw();
    
    tableInteracciones = $('#tablaInteracciones').DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
        },
        // pageLength : 5,
        lengthMenu: [[5,10, 20, -1], [5,10, 20, 'Todos']],
        
    });
});
function eliminarrol(idrol){
    if(confirm('Esta acción no podrá deshacerse. ¿Continuar?')){

        document.getElementById('delete-form_'+idrol).submit();
    }
}
</script>
@endsection

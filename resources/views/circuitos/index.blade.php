@extends('layout')

@section('title', 'Circuitos')

@section('seccionSaludo')
<h1>Listado de Circuitos</h1>
@endsection

@section('contenido')

    <div class="col-md-12">

        <div class="table-responsive" id="seccionTabla">
            <table class="table table-striped table-dark table-sm" id="tablecircuito">
                <thead class="thead-dark">
                <tr>
                    <th>Circuito</th>
                    <th> Padron </th>
                </tr>
                </thead>
                <tbody>
                    @foreach($circuitos as $circuito)  
                        <tr>
                            <td>{{ $circuito->circuito_nombre}}</td>
                            <td>
                                @can('padron-descargar')                                    
                                    @if ($circuito->pdf != null)                                    
                                        <a href="{{ $circuito->pdf }}" download  data-toggle="tooltip" data-placement="left" title="Descargar Padron"><i class="fas fa-cloud-download-alt fa-2x"></i></a>                               
                                    @endif
                                @endcan
                                @can('padron-subir') 
                                    <a href="javascript:;" style="margin-left: 1%" class="cargarPadron" id="cargarPadron_{{ $circuito->idcircuito}}"
                                        data-toggle="tooltip" data-placement="right" title="Subir Padron"><i class="fas fa-cloud-upload-alt fa-2x"></i></a>
                                @endcan
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

        </div>

    </div>

@endsection

@section('script')
<script>
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();
});
$(document).ready(function() {
    setTimeout(function() {
      $('#message').fadeOut('fast');
    }, 3500);

    const TCircuitos = $('#tablecircuito').DataTable({
        "order": [],
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
        },
        // pageLength : 5,
        lengthMenu: [[10, 20, -1], [10, 20, 'Todos']]
        
    });
    TCircuitos.page.len(20).draw();
});
$(document).on('click','.cargarPadron',function () {
    let idcircuito = $(this).prop('id').split('_')[1];
    $('#idcircuito').val(idcircuito)
    $('#ModalPadron').modal('show');
});
$(document).on('click','#PadronCerrar',function () {
    $('#fromCargarPadron')[0].reset();
    $('#ModalPadron').modal('hide');
});
$(document).on('click','#PadronAceptar',function () {
    $('#fromCargarPadron').submit();
});
</script>
@endsection

@extends('layout')

@php
    use App\User;
@endphp

@section('title', 'Padron')
@section('seccionSaludo')
<h1>Listado de Padron</h1>
<h3 class="title text-center">---</h3>
@endsection

@section('contenido')
    <div class="col-12">
        <div class="row">
            
            {{-- <a href="{{route('padron.create')}}"; class="btn btn-success" style="margin-left: 89%;">Nuevo registro</a> --}}
            
        </div>
        <div class="table-responsive" id="seccionTabla">
            <table class="table table-striped table-dark table-sm" id="tablePadron">
                <thead class="thead-dark">
                <tr>
                    <th>Tipo Ejemplar</th>
                    <th>Nombre y Apellido</th>
                    <th>domicilio</th>
                    <th>Circuito</th>
                    <th>Localidad</th>
                    <th>Departamento</th>
                    <th>Seccion</th>
                    <th>sexo</th>
                    <th>mesa</th>
                    <th>Orden Mesa</th>
                    <th>Establecimiento</th>
                    <th>Domicilio establec</th>
                    {{-- <th></th>
                    <th></th> --}}
                </tr>
                </thead>
                <tbody>
                   
                </tbody>
            </table>

        </div>

    </div>


@endsection

@section('script')
<script>

$(document).ready(function() {
    setTimeout(function() {
      $('#message').fadeOut('fast');
    }, 3500);

    const Tpadron = $('#tablePadron').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'pdfHtml5',
        ],
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
        },
        // pageLength : 5,
        lengthMenu: [[10, 20, -1], [10, 20, 'Todos']],
        processing:true,
        serverSide:true,
        ajax: "{{ route('padron.ajax') }}",
        columns: [
            {data : 'tipo_ejemplar'},
            {data : 'NomApe'},
            {data : 'domicilio'},
            {data : 'Circuito2'},
            {data : 'Localidad'},
            {data : 'Departamento'},
            {data : 'Seccion'},
            {data : 'sexo'},
            {data : 'mesa'},
            {data : 'orden_mesa'},
            {data : 'establecim'},
            {data : 'domic_establec'},
           /*  {data : 'btnEdit'},
            {data : 'btnDelete'}, */
        ]
        
    });
    Tpadron.page.len(20).draw();

});


function eliminarpadron(idpadron){
    if(confirm('Esta acción no podrá deshacerse. ¿Continuar?')){

        document.getElementById('delete-form_'+idpadron).submit();
    }
}

</script>
@endsection

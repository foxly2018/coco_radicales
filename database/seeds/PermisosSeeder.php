<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class PermisosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permisos_admin = [];
        $permisos_miembro = [];

        $mod_voluntaios =  Permission::create(['name' => 'mod-voluntaios']);
        $mod_dirigentes =  Permission::create(['name' => 'mod-dirigentes']);
        $mod_asig_circuitos =  Permission::create(['name' => 'mod-asignar-circuitos']);

        array_push($permisos_admin, $mod_voluntaios);
        array_push($permisos_admin, $mod_dirigentes);
        array_push($permisos_admin, $mod_asig_circuitos);
        array_push($permisos_admin, Permission::create(['name' => 'mod-miembros']));
        array_push($permisos_admin, Permission::create(['name' => 'mod-usuarios']));
        array_push($permisos_admin, Permission::create(['name' => 'mod-roles-permisos']));
        $roleAdmin = Role::create(['name' => 'Administrador']);
        $roleAdmin->syncPermissions($permisos_admin);

        array_push($permisos_miembro, $mod_voluntaios);
        array_push($permisos_miembro, $mod_dirigentes);
        array_push($permisos_miembro, $mod_asig_circuitos);
        $roleMiembro = Role::create(['name' => 'Miembro']);
        $roleMiembro->syncPermissions($permisos_miembro);

        $roleDirigente = Role::create(['name' => 'Dirigente']);
        $roleDirigente->givePermissionTo($mod_voluntaios);

        /* PERMISOS PRA LOCALIDADES */

        /* Permission::create([
            'name'=>'Municipio Burruyacu',
            'guard_name' => 'p_localidad'
        ]); */

        /* QUERY PARA CORRER LOS NUEVOS PERMISOS */
        /*      
        INSERT INTO `ciatware_voluntarios`.`permissions` (`name`, `guard_name`, `created_at`, `updated_at`) VALUES ('mod-llamadas', 'web', '2021-04-30 02:42:19', '2021-04-30 02:42:19');
        INSERT INTO `ciatware_voluntarios`.`permissions` (`name`, `guard_name`, `created_at`, `updated_at`) VALUES ('voluntario-alta', 'web', '2021-04-30 02:42:19', '2021-04-30 02:42:19');
        INSERT INTO `ciatware_voluntarios`.`permissions` (`name`, `guard_name`, `created_at`, `updated_at`) VALUES ('voluntario-edicion', 'web', '2021-04-30 02:42:19', '2021-04-30 02:42:19');
        INSERT INTO `ciatware_voluntarios`.`permissions` (`name`, `guard_name`, `created_at`, `updated_at`) VALUES ('voluntario-eliminar', 'web', '2021-04-30 02:42:19', '2021-04-30 02:42:19');
        INSERT INTO `ciatware_voluntarios`.`permissions` (`name`, `guard_name`, `created_at`, `updated_at`) VALUES ('voluntario-detalle', 'web', '2021-04-30 02:42:19', '2021-04-30 02:42:19');
        INSERT INTO `ciatware_voluntarios`.`permissions` (`name`, `guard_name`, `created_at`, `updated_at`) VALUES ('voluntario-alta-interaccion', 'web', '2021-04-30 02:42:19', '2021-04-30 02:42:19');
        INSERT INTO `ciatware_voluntarios`.`permissions` (`name`, `guard_name`, `created_at`, `updated_at`) VALUES ('dirigente-alta', 'web', '2021-04-30 02:42:19', '2021-04-30 02:42:19');
        INSERT INTO `ciatware_voluntarios`.`permissions` (`name`, `guard_name`, `created_at`, `updated_at`) VALUES ('dirigente-edicion', 'web', '2021-04-30 02:42:19', '2021-04-30 02:42:19');
        INSERT INTO `ciatware_voluntarios`.`permissions` (`name`, `guard_name`, `created_at`, `updated_at`) VALUES ('dirigente-eliminar', 'web', '2021-04-30 02:42:19', '2021-04-30 02:42:19');
        INSERT INTO `ciatware_voluntarios`.`permissions` (`name`, `guard_name`, `created_at`, `updated_at`) VALUES ('dirigente-detalle', 'web', '2021-04-30 02:42:19', '2021-04-30 02:42:19');
        INSERT INTO `ciatware_voluntarios`.`permissions` (`name`, `guard_name`, `created_at`, `updated_at`) VALUES ('coordinador-alta', 'web', '2021-04-30 02:42:19', '2021-04-30 02:42:19');
        INSERT INTO `ciatware_voluntarios`.`permissions` (`name`, `guard_name`, `created_at`, `updated_at`) VALUES ('coordinador-edicion', 'web', '2021-04-30 02:42:19', '2021-04-30 02:42:19');
        INSERT INTO `ciatware_voluntarios`.`permissions` (`name`, `guard_name`, `created_at`, `updated_at`) VALUES ('coordinador-eliminar', 'web', '2021-04-30 02:42:19', '2021-04-30 02:42:19');
        INSERT INTO `ciatware_voluntarios`.`permissions` (`name`, `guard_name`, `created_at`, `updated_at`) VALUES ('coordinador-detalle', 'web', '2021-04-30 02:42:19', '2021-04-30 02:42:19');
        INSERT INTO `ciatware_voluntarios`.`permissions` (`name`, `guard_name`, `created_at`, `updated_at`) VALUES ('padron-subir', 'web', '2021-04-30 02:42:19', '2021-04-30 02:42:19');
        INSERT INTO `ciatware_voluntarios`.`permissions` (`name`, `guard_name`, `created_at`, `updated_at`) VALUES ('padron-descargar', 'web', '2021-04-30 02:42:19', '2021-04-30 02:42:19');
        INSERT INTO `ciatware_voluntarios`.`permissions` (`name`, `guard_name`, `created_at`, `updated_at`) VALUES ('usuario-alta', 'web', '2021-04-30 02:42:19', '2021-04-30 02:42:19');
        INSERT INTO `ciatware_voluntarios`.`permissions` (`name`, `guard_name`, `created_at`, `updated_at`) VALUES ('usuario-edicion', 'web', '2021-04-30 02:42:19', '2021-04-30 02:42:19');
        INSERT INTO `ciatware_voluntarios`.`permissions` (`name`, `guard_name`, `created_at`, `updated_at`) VALUES ('usuario-eliminar', 'web', '2021-04-30 02:42:19', '2021-04-30 02:42:19');
        INSERT INTO `ciatware_voluntarios`.`permissions` (`name`, `guard_name`, `created_at`, `updated_at`) VALUES ('usuario-detalle', 'web', '2021-04-30 02:42:19', '2021-04-30 02:42:19');
        INSERT INTO `ciatware_voluntarios`.`permissions` (`name`, `guard_name`, `created_at`, `updated_at`) VALUES ('rol-alta', 'web', '2021-04-30 02:42:19', '2021-04-30 02:42:19');
        INSERT INTO `ciatware_voluntarios`.`permissions` (`name`, `guard_name`, `created_at`, `updated_at`) VALUES ('rol-edicion', 'web', '2021-04-30 02:42:19', '2021-04-30 02:42:19');
        INSERT INTO `ciatware_voluntarios`.`permissions` (`name`, `guard_name`, `created_at`, `updated_at`) VALUES ('rol-eliminar', 'web', '2021-04-30 02:42:19', '2021-04-30 02:42:19'); 
        */

        /* RELACION ROL PERMISOS ADMIN */
        /* 
        INSERT INTO `ciatware_voluntarios`.`role_has_permissions` (`permission_id`, `role_id`) VALUES ('165', '1');
        INSERT INTO `ciatware_voluntarios`.`role_has_permissions` (`permission_id`, `role_id`) VALUES ('166', '1');
        INSERT INTO `ciatware_voluntarios`.`role_has_permissions` (`permission_id`, `role_id`) VALUES ('167', '1');
        INSERT INTO `ciatware_voluntarios`.`role_has_permissions` (`permission_id`, `role_id`) VALUES ('168', '1');
        INSERT INTO `ciatware_voluntarios`.`role_has_permissions` (`permission_id`, `role_id`) VALUES ('169', '1');
        INSERT INTO `ciatware_voluntarios`.`role_has_permissions` (`permission_id`, `role_id`) VALUES ('170', '1');
        INSERT INTO `ciatware_voluntarios`.`role_has_permissions` (`permission_id`, `role_id`) VALUES ('171', '1');
        INSERT INTO `ciatware_voluntarios`.`role_has_permissions` (`permission_id`, `role_id`) VALUES ('172', '1');
        INSERT INTO `ciatware_voluntarios`.`role_has_permissions` (`permission_id`, `role_id`) VALUES ('173', '1');
        INSERT INTO `ciatware_voluntarios`.`role_has_permissions` (`permission_id`, `role_id`) VALUES ('174', '1');
        INSERT INTO `ciatware_voluntarios`.`role_has_permissions` (`permission_id`, `role_id`) VALUES ('175', '1');
        INSERT INTO `ciatware_voluntarios`.`role_has_permissions` (`permission_id`, `role_id`) VALUES ('176', '1');
        INSERT INTO `ciatware_voluntarios`.`role_has_permissions` (`permission_id`, `role_id`) VALUES ('177', '1');
        INSERT INTO `ciatware_voluntarios`.`role_has_permissions` (`permission_id`, `role_id`) VALUES ('178', '1');
        INSERT INTO `ciatware_voluntarios`.`role_has_permissions` (`permission_id`, `role_id`) VALUES ('179', '1');
        INSERT INTO `ciatware_voluntarios`.`role_has_permissions` (`permission_id`, `role_id`) VALUES ('180', '1');
        INSERT INTO `ciatware_voluntarios`.`role_has_permissions` (`permission_id`, `role_id`) VALUES ('181', '1');
        INSERT INTO `ciatware_voluntarios`.`role_has_permissions` (`permission_id`, `role_id`) VALUES ('182', '1');
        INSERT INTO `ciatware_voluntarios`.`role_has_permissions` (`permission_id`, `role_id`) VALUES ('183', '1');
        INSERT INTO `ciatware_voluntarios`.`role_has_permissions` (`permission_id`, `role_id`) VALUES ('184', '1');
        INSERT INTO `ciatware_voluntarios`.`role_has_permissions` (`permission_id`, `role_id`) VALUES ('185', '1');
        INSERT INTO `ciatware_voluntarios`.`role_has_permissions` (`permission_id`, `role_id`) VALUES ('186', '1');
        INSERT INTO `ciatware_voluntarios`.`role_has_permissions` (`permission_id`, `role_id`) VALUES ('187', '1');

        */

    }
}

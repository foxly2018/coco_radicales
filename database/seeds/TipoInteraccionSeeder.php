<?php

use Illuminate\Database\Seeder;
use App\TipoInteraccion;

class TipoInteraccionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TipoInteraccion::create([
            'tipo_nombre'=>'Llamada',
            'estado'=>1
        ]);
        TipoInteraccion::create([
            'tipo_nombre'=>'Personal',
            'estado'=>1
        ]);
        TipoInteraccion::create([
            'tipo_nombre'=>'Mensaje Celular',
            'estado'=>1
        ]);
        TipoInteraccion::create([
            'tipo_nombre'=>'red social',
            'estado'=>1
        ]);
        TipoInteraccion::create([
            'tipo_nombre'=>'mail',
            'estado'=>1
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;
use App\Direccion;

class DireccionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Direccion::create([
            'calle'=>'el ceibo',
            'numero'=>'766',
            'piso'=>'0',
            'dpto'=>'0',
            'manzana'=>'0',
            'casa'=>'0',
            'localidad_idlocalidad'=>6
        ]);
    }
}

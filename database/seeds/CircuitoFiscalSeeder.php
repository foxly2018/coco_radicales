<?php

use App\CircuitoFiscal;
use Illuminate\Database\Seeder;

class CircuitoFiscalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CircuitoFiscal::create(['circuito_nombre'=>'1', 'localidad_idlocalidad'=>12, 'cantidadEscuelas'=>2,'cantidadMesas'=>18,'disponible'=>'no','estado'=>'incompleto']);
CircuitoFiscal::create(['circuito_nombre'=>'1A', 'localidad_idlocalidad'=>12, 'cantidadEscuelas'=>2,'cantidadMesas'=>15,'disponible'=>'no','estado'=>'incompleto']);
CircuitoFiscal::create(['circuito_nombre'=>'2', 'localidad_idlocalidad'=>12, 'cantidadEscuelas'=>2,'cantidadMesas'=>11,'disponible'=>'no','estado'=>'incompleto']);
CircuitoFiscal::create(['circuito_nombre'=>'2A', 'localidad_idlocalidad'=>12, 'cantidadEscuelas'=>2,'cantidadMesas'=>12,'disponible'=>'no','estado'=>'incompleto']);
CircuitoFiscal::create(['circuito_nombre'=>'3', 'localidad_idlocalidad'=>12, 'cantidadEscuelas'=>2,'cantidadMesas'=>20,'disponible'=>'no','estado'=>'incompleto']);
CircuitoFiscal::create(['circuito_nombre'=>'4', 'localidad_idlocalidad'=>12, 'cantidadEscuelas'=>2,'cantidadMesas'=>19,'disponible'=>'no','estado'=>'incompleto']);
CircuitoFiscal::create(['circuito_nombre'=>'5', 'localidad_idlocalidad'=>12, 'cantidadEscuelas'=>4,'cantidadMesas'=>32,'disponible'=>'si','estado'=>'incompleto']);
CircuitoFiscal::create(['circuito_nombre'=>'6', 'localidad_idlocalidad'=>12, 'cantidadEscuelas'=>1,'cantidadMesas'=>15,'disponible'=>'si','estado'=>'incompleto']);
CircuitoFiscal::create(['circuito_nombre'=>'7', 'localidad_idlocalidad'=>12, 'cantidadEscuelas'=>2,'cantidadMesas'=>14,'disponible'=>'no','estado'=>'incompleto']);
CircuitoFiscal::create(['circuito_nombre'=>'7A', 'localidad_idlocalidad'=>12, 'cantidadEscuelas'=>2,'cantidadMesas'=>18,'disponible'=>'si','estado'=>'incompleto']);
CircuitoFiscal::create(['circuito_nombre'=>'8', 'localidad_idlocalidad'=>12, 'cantidadEscuelas'=>2,'cantidadMesas'=>14,'disponible'=>'no','estado'=>'incompleto']);
CircuitoFiscal::create(['circuito_nombre'=>'8A', 'localidad_idlocalidad'=>12, 'cantidadEscuelas'=>3,'cantidadMesas'=>21,'disponible'=>'no','estado'=>'incompleto']);
CircuitoFiscal::create(['circuito_nombre'=>'9', 'localidad_idlocalidad'=>12, 'cantidadEscuelas'=>4,'cantidadMesas'=>33,'disponible'=>'si','estado'=>'incompleto']);
CircuitoFiscal::create(['circuito_nombre'=>'9A', 'localidad_idlocalidad'=>12, 'cantidadEscuelas'=>3,'cantidadMesas'=>31,'disponible'=>'no','estado'=>'incompleto']);
CircuitoFiscal::create(['circuito_nombre'=>'10', 'localidad_idlocalidad'=>12, 'cantidadEscuelas'=>3,'cantidadMesas'=>34,'disponible'=>'no','estado'=>'incompleto']);
CircuitoFiscal::create(['circuito_nombre'=>'10A', 'localidad_idlocalidad'=>12, 'cantidadEscuelas'=>7,'cantidadMesas'=>60,'disponible'=>'si','estado'=>'incompleto']);
CircuitoFiscal::create(['circuito_nombre'=>'11', 'localidad_idlocalidad'=>12, 'cantidadEscuelas'=>1,'cantidadMesas'=>6,'disponible'=>'no','estado'=>'incompleto']);
CircuitoFiscal::create(['circuito_nombre'=>'11A', 'localidad_idlocalidad'=>12, 'cantidadEscuelas'=>3,'cantidadMesas'=>26,'disponible'=>'si','estado'=>'incompleto']);
CircuitoFiscal::create(['circuito_nombre'=>'12', 'localidad_idlocalidad'=>12, 'cantidadEscuelas'=>3,'cantidadMesas'=>33,'disponible'=>'no','estado'=>'incompleto']);
CircuitoFiscal::create(['circuito_nombre'=>'12A', 'localidad_idlocalidad'=>12, 'cantidadEscuelas'=>3,'cantidadMesas'=>26,'disponible'=>'no','estado'=>'incompleto']);
CircuitoFiscal::create(['circuito_nombre'=>'13', 'localidad_idlocalidad'=>12, 'cantidadEscuelas'=>3,'cantidadMesas'=>32,'disponible'=>'si','estado'=>'incompleto']);
CircuitoFiscal::create(['circuito_nombre'=>'13A', 'localidad_idlocalidad'=>12, 'cantidadEscuelas'=>2,'cantidadMesas'=>13,'disponible'=>'si','estado'=>'incompleto']);
CircuitoFiscal::create(['circuito_nombre'=>'14', 'localidad_idlocalidad'=>12, 'cantidadEscuelas'=>6,'cantidadMesas'=>55,'disponible'=>'no','estado'=>'incompleto']);
CircuitoFiscal::create(['circuito_nombre'=>'14A', 'localidad_idlocalidad'=>12, 'cantidadEscuelas'=>1,'cantidadMesas'=>9,'disponible'=>'no','estado'=>'incompleto']);
CircuitoFiscal::create(['circuito_nombre'=>'14B', 'localidad_idlocalidad'=>12, 'cantidadEscuelas'=>1,'cantidadMesas'=>10,'disponible'=>'no','estado'=>'incompleto']);
CircuitoFiscal::create(['circuito_nombre'=>'14C', 'localidad_idlocalidad'=>12, 'cantidadEscuelas'=>1,'cantidadMesas'=>18,'disponible'=>'si','estado'=>'incompleto']);
CircuitoFiscal::create(['circuito_nombre'=>'14D', 'localidad_idlocalidad'=>12, 'cantidadEscuelas'=>2,'cantidadMesas'=>13,'disponible'=>'no','estado'=>'incompleto']);
CircuitoFiscal::create(['circuito_nombre'=>'15', 'localidad_idlocalidad'=>12, 'cantidadEscuelas'=>5,'cantidadMesas'=>39,'disponible'=>'si','estado'=>'incompleto']);
CircuitoFiscal::create(['circuito_nombre'=>'15A', 'localidad_idlocalidad'=>12, 'cantidadEscuelas'=>2,'cantidadMesas'=>33,'disponible'=>'si','estado'=>'incompleto']);
CircuitoFiscal::create(['circuito_nombre'=>'15B', 'localidad_idlocalidad'=>12, 'cantidadEscuelas'=>6,'cantidadMesas'=>62,'disponible'=>'si','estado'=>'incompleto']);
CircuitoFiscal::create(['circuito_nombre'=>'16', 'localidad_idlocalidad'=>12, 'cantidadEscuelas'=>4,'cantidadMesas'=>40,'disponible'=>'no','estado'=>'incompleto']);
CircuitoFiscal::create(['circuito_nombre'=>'16A', 'localidad_idlocalidad'=>12, 'cantidadEscuelas'=>4,'cantidadMesas'=>36,'disponible'=>'si','estado'=>'incompleto']);
CircuitoFiscal::create(['circuito_nombre'=>'17', 'localidad_idlocalidad'=>12, 'cantidadEscuelas'=>5,'cantidadMesas'=>46,'disponible'=>'si','estado'=>'incompleto']);
CircuitoFiscal::create(['circuito_nombre'=>'17A', 'localidad_idlocalidad'=>12, 'cantidadEscuelas'=>5,'cantidadMesas'=>34,'disponible'=>'si','estado'=>'incompleto']);
CircuitoFiscal::create(['circuito_nombre'=>'18', 'localidad_idlocalidad'=>12, 'cantidadEscuelas'=>5,'cantidadMesas'=>48,'disponible'=>'no','estado'=>'incompleto']);
CircuitoFiscal::create(['circuito_nombre'=>'18A', 'localidad_idlocalidad'=>12, 'cantidadEscuelas'=>1,'cantidadMesas'=>11,'disponible'=>'no','estado'=>'incompleto']);
CircuitoFiscal::create(['circuito_nombre'=>'18B', 'localidad_idlocalidad'=>12, 'cantidadEscuelas'=>2,'cantidadMesas'=>28,'disponible'=>'no','estado'=>'incompleto']);
CircuitoFiscal::create(['circuito_nombre'=>'18C', 'localidad_idlocalidad'=>12, 'cantidadEscuelas'=>2,'cantidadMesas'=>28,'disponible'=>'no','estado'=>'incompleto']);
CircuitoFiscal::create(['circuito_nombre'=>'18D', 'localidad_idlocalidad'=>12, 'cantidadEscuelas'=>2,'cantidadMesas'=>17,'disponible'=>'no','estado'=>'incompleto']);
CircuitoFiscal::create(['circuito_nombre'=>'18E', 'localidad_idlocalidad'=>12, 'cantidadEscuelas'=>2,'cantidadMesas'=>29,'disponible'=>'no','estado'=>'incompleto']);
CircuitoFiscal::create(['circuito_nombre'=>'18F', 'localidad_idlocalidad'=>12, 'cantidadEscuelas'=>1,'cantidadMesas'=>14,'disponible'=>'no','estado'=>'incompleto']);
CircuitoFiscal::create(['circuito_nombre'=>'18G', 'localidad_idlocalidad'=>12, 'cantidadEscuelas'=>2,'cantidadMesas'=>17,'disponible'=>'no','estado'=>'incompleto']);
CircuitoFiscal::create(['circuito_nombre'=>'19', 'localidad_idlocalidad'=>12, 'cantidadEscuelas'=>4,'cantidadMesas'=>44,'disponible'=>'si','estado'=>'incompleto']);
CircuitoFiscal::create(['circuito_nombre'=>'19A', 'localidad_idlocalidad'=>12, 'cantidadEscuelas'=>4,'cantidadMesas'=>31,'disponible'=>'no','estado'=>'incompleto']);
CircuitoFiscal::create(['circuito_nombre'=>'20', 'localidad_idlocalidad'=>12, 'cantidadEscuelas'=>7,'cantidadMesas'=>71,'disponible'=>'no','estado'=>'incompleto']);
CircuitoFiscal::create(['circuito_nombre'=>'21', 'localidad_idlocalidad'=>12, 'cantidadEscuelas'=>6,'cantidadMesas'=>59,'disponible'=>'si','estado'=>'incompleto']);
CircuitoFiscal::create(['circuito_nombre'=>'22', 'localidad_idlocalidad'=>12, 'cantidadEscuelas'=>2,'cantidadMesas'=>21,'disponible'=>'no','estado'=>'incompleto']);

    }
}

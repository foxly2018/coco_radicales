<?php

use App\Escuela;
use Illuminate\Database\Seeder;

class EscuelaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Escuela::create(['nombre_escuela'=>'ESC LICEO VOCACIONAL SARMIENTO','circuitofiscal'=>1,'numero_mesas'=>9,'faltan_mesas'=>9,'direccion'=>'VIRGEN DE LA MERCED 29','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESC NORMAL JUAN B ALBERDI','circuitofiscal'=>1,'numero_mesas'=>9,'faltan_mesas'=>9,'direccion'=>'MUÑECAS 219','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'COL NUESTRA SRA. DE LA MERCED','circuitofiscal'=>2,'numero_mesas'=>9,'faltan_mesas'=>9,'direccion'=>'LAS HERAS 14','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA OBISPO MOLINA','circuitofiscal'=>2,'numero_mesas'=>6,'faltan_mesas'=>6,'direccion'=>'CRISOSTOMO ÁLVAREZ 334','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA 9 DE JULIO','circuitofiscal'=>3,'numero_mesas'=>6,'faltan_mesas'=>6,'direccion'=>'CATAMARCA 112','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA BERNARDINO RIVADAVIA','circuitofiscal'=>3,'numero_mesas'=>5,'faltan_mesas'=>5,'direccion'=>'SALTA ESQ. 24 DE SETIEMBRE','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'INSTITUTO PRIVADO TUCUMÁN','circuitofiscal'=>4,'numero_mesas'=>6,'faltan_mesas'=>6,'direccion'=>'CRISOSTOMO ÁLVAREZ 668','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'COLEGIO BELGRANO','circuitofiscal'=>4,'numero_mesas'=>6,'faltan_mesas'=>6,'direccion'=>'CHACABUCO 361','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'COLEGIO GUILLERMINA L.D GUZMÁN','circuitofiscal'=>5,'numero_mesas'=>10,'faltan_mesas'=>10,'direccion'=>'AV SAENZ PEÑA 637','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA MONTEAGUDO','circuitofiscal'=>5,'numero_mesas'=>10,'faltan_mesas'=>10,'direccion'=>'MORENO ESQ. GENERAL PAZ','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'COLEGIO DE LAS ESCLAVAS','circuitofiscal'=>6,'numero_mesas'=>10,'faltan_mesas'=>10,'direccion'=>'ALBERDI 509','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'INSTITUTO TÉCNICO U.N.T.','circuitofiscal'=>6,'numero_mesas'=>9,'faltan_mesas'=>9,'direccion'=>'GENERAL PAZ 920','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA BARTOLOME MITRE','circuitofiscal'=>7,'numero_mesas'=>10,'faltan_mesas'=>10,'direccion'=>'SANTIAGO ESQ. MUÑECAS','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA JOSÉ MARMOL','circuitofiscal'=>7,'numero_mesas'=>10,'faltan_mesas'=>10,'direccion'=>'SANTIAGO ESQ. VIRGEN DE LA MERCED','disponible'=>'si','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'INSTITUTO GUIDO SPANO','circuitofiscal'=>7,'numero_mesas'=>6,'faltan_mesas'=>6,'direccion'=>'LAPRIDA 464','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'INSTITUTO SAN MIGUEL','circuitofiscal'=>7,'numero_mesas'=>6,'faltan_mesas'=>6,'direccion'=>'MONTEAGUDO 341','disponible'=>'si','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'COLEGIO SAGRADO CORAZÓN','circuitofiscal'=>8,'numero_mesas'=>15,'faltan_mesas'=>15,'direccion'=>'25 DE MAYO 680','disponible'=>'si','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'COLEGIO TULIO GARCÍA FERNÁNDEZ','circuitofiscal'=>9,'numero_mesas'=>7,'faltan_mesas'=>7,'direccion'=>'AVDA. MITRE 312','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'INSTITUTO TECNICO LORENZO MASSA','circuitofiscal'=>9,'numero_mesas'=>7,'faltan_mesas'=>7,'direccion'=>'12 DE OCTUBRE 351','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA RAÚL COLOMBRES','circuitofiscal'=>10,'numero_mesas'=>9,'faltan_mesas'=>9,'direccion'=>'MARCOS PAZ 1425','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA TÉCNICA Nº3','circuitofiscal'=>10,'numero_mesas'=>9,'faltan_mesas'=>9,'direccion'=>'AVDA. MITRE 746','disponible'=>'si','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESC PATRICIAS ARGENTINAS','circuitofiscal'=>11,'numero_mesas'=>7,'faltan_mesas'=>7,'direccion'=>'AVDA. MATE DE LUNA 2051','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESC DE COMERCIO GRAL URQUIZA','circuitofiscal'=>11,'numero_mesas'=>7,'faltan_mesas'=>7,'direccion'=>'SAN MARTÍN 2050','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA Nº 259 (PRIMARIA)','circuitofiscal'=>12,'numero_mesas'=>9,'faltan_mesas'=>9,'direccion'=>'LIBERTAD 660','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA GRAL BELGRANO-SECUNDAR','circuitofiscal'=>12,'numero_mesas'=>8,'faltan_mesas'=>8,'direccion'=>'LIBERTAD 630','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'INSTITUTO PRIVADO SAN PABLO','circuitofiscal'=>12,'numero_mesas'=>4,'faltan_mesas'=>4,'direccion'=>'LAVALLE  1775','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA CAPITAL FEDERAL','circuitofiscal'=>13,'numero_mesas'=>8,'faltan_mesas'=>8,'direccion'=>'MATHEU ESQ. LAS HERAS','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA AGUSTIN J DE LA VEGA','circuitofiscal'=>13,'numero_mesas'=>10,'faltan_mesas'=>10,'direccion'=>'FLORIDA 663','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA LEANDRO ALEM','circuitofiscal'=>13,'numero_mesas'=>9,'faltan_mesas'=>9,'direccion'=>'CHACABUCO ESQ FLORIDA','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA ESPECIAL LUIS BRAILLE','circuitofiscal'=>13,'numero_mesas'=>6,'faltan_mesas'=>6,'direccion'=>'ALSINA Y BUENOS AIRES','disponible'=>'si','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA PRESIDENTE AVELLANEDA','circuitofiscal'=>14,'numero_mesas'=>11,'faltan_mesas'=>11,'direccion'=>'LA RIOJA ESQ. AVDA. ROCA','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'INSTITUTO NICOLAS AVELLANEDA','circuitofiscal'=>14,'numero_mesas'=>10,'faltan_mesas'=>10,'direccion'=>'LA RIOJA 1060','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA SECUNDARIA JUAN LARREA','circuitofiscal'=>14,'numero_mesas'=>10,'faltan_mesas'=>10,'direccion'=>'ALBERDI 1300','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA BENJAMIN MATIENZO','circuitofiscal'=>15,'numero_mesas'=>17,'faltan_mesas'=>17,'direccion'=>'AVDA. BRIGIDO TERAN 600','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA DELFIN GIGENA','circuitofiscal'=>15,'numero_mesas'=>10,'faltan_mesas'=>10,'direccion'=>'JULIO PREBISCH 600','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA MEDIA Bº LOLA MORA','circuitofiscal'=>15,'numero_mesas'=>7,'faltan_mesas'=>7,'direccion'=>'CALLE 3 S/N','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'COLEGIO SAN CAYETANO','circuitofiscal'=>16,'numero_mesas'=>14,'faltan_mesas'=>14,'direccion'=>'PEDRO MIGUEL ARAOZ 207','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA ALFONSINA STORNI SECUND.','circuitofiscal'=>16,'numero_mesas'=>5,'faltan_mesas'=>5,'direccion'=>'TOMAS EDISON 373','disponible'=>'si','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA LOLA MORA','circuitofiscal'=>16,'numero_mesas'=>10,'faltan_mesas'=>10,'direccion'=>'EUGENIO MENDEZ 472','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA ALFONSINA STORNI PRIMA','circuitofiscal'=>16,'numero_mesas'=>8,'faltan_mesas'=>8,'direccion'=>'RICARDO ROJAS ESQ. PIO XII','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA MARINA ALFARO','circuitofiscal'=>16,'numero_mesas'=>9,'faltan_mesas'=>9,'direccion'=>'MARINA ALFARO 1200','disponible'=>'si','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESC TECNICA 2 OBISPO COLOMBRES','circuitofiscal'=>16,'numero_mesas'=>9,'faltan_mesas'=>9,'direccion'=>'DOMINGO GARCIA 47','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'CLUB SOCIAL Y DEP SAN CAYETANO','circuitofiscal'=>16,'numero_mesas'=>5,'faltan_mesas'=>5,'direccion'=>'GUTEMBERG 220','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA SAN MARTIN','circuitofiscal'=>17,'numero_mesas'=>6,'faltan_mesas'=>6,'direccion'=>'LAPRIDA 339','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA BERNABE ARAOZ','circuitofiscal'=>18,'numero_mesas'=>10,'faltan_mesas'=>10,'direccion'=>'AV BENJAMIN ARAOZ 1067','disponible'=>'si','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA BERNABE ARAOZ (SECUND)','circuitofiscal'=>18,'numero_mesas'=>9,'faltan_mesas'=>9,'direccion'=>'PJE TREJO Y SANABRIA 950','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'FACULTAD DE EDUCACION FISICA UNT','circuitofiscal'=>18,'numero_mesas'=>7,'faltan_mesas'=>7,'direccion'=>'AV BENJAMIN ARAOZ 751','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'UNIVERSIDAD TECNOLOGICA NAC.','circuitofiscal'=>19,'numero_mesas'=>10,'faltan_mesas'=>10,'direccion'=>'RIVADAVIA 1050','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'COLEGIO SANTA CATALINA','circuitofiscal'=>19,'numero_mesas'=>10,'faltan_mesas'=>10,'direccion'=>'AVDA. SARMIENTO 253','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA DE COMERCIO Nº1','circuitofiscal'=>19,'numero_mesas'=>13,'faltan_mesas'=>13,'direccion'=>'AVDA. SARMIENTO 391','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA EMILIO CASTELLAR - SECUND.','circuitofiscal'=>20,'numero_mesas'=>7,'faltan_mesas'=>7,'direccion'=>'MUÑECAS 2500','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA LUCAS CORDOBA','circuitofiscal'=>20,'numero_mesas'=>11,'faltan_mesas'=>11,'direccion'=>'RIVADAVIA ESQ. MADRID','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'CLUB ATLETICO TUCUMAN','circuitofiscal'=>20,'numero_mesas'=>8,'faltan_mesas'=>8,'direccion'=>'25 DE MAYO Y CHILE','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'COLEGIO NAC. BARTOLOME MITRE','circuitofiscal'=>21,'numero_mesas'=>11,'faltan_mesas'=>11,'direccion'=>'MUÑECAS 850','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA PRESIDENTE ROCA','circuitofiscal'=>21,'numero_mesas'=>10,'faltan_mesas'=>10,'direccion'=>'ESPAÑA 507','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA PADRE ROQUE CORREA','circuitofiscal'=>21,'numero_mesas'=>11,'faltan_mesas'=>11,'direccion'=>'MAIPU 657','disponible'=>'si','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA ARROYO Y PINEDO','circuitofiscal'=>22,'numero_mesas'=>8,'faltan_mesas'=>8,'direccion'=>'AVDA. REPUBLICA DEL LIBANO 1975','disponible'=>'si','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA ESPECIAL EL TALLER','circuitofiscal'=>22,'numero_mesas'=>5,'faltan_mesas'=>5,'direccion'=>'PJE COMANDANTE ESPORA 1565','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA RICARDO GUTIERREZ','circuitofiscal'=>23,'numero_mesas'=>10,'faltan_mesas'=>10,'direccion'=>'PERU 577','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA NAC Nº49','circuitofiscal'=>23,'numero_mesas'=>10,'faltan_mesas'=>10,'direccion'=>'MARTIN BERHO 400','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA BENJAMIN VILLAFAÑE','circuitofiscal'=>23,'numero_mesas'=>10,'faltan_mesas'=>10,'direccion'=>'BLAS PARERA 700','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA ELMINA PAZ DE GALLO','circuitofiscal'=>23,'numero_mesas'=>10,'faltan_mesas'=>10,'direccion'=>'ALVAREZ CONDARCO 50','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA SECUNDARIA BLAS PARERA','circuitofiscal'=>23,'numero_mesas'=>8,'faltan_mesas'=>8,'direccion'=>'BLAS PARERA 480','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'CLUB SPORTIVO GUZMAN','circuitofiscal'=>23,'numero_mesas'=>7,'faltan_mesas'=>7,'direccion'=>'JURAMENTO 1000','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'COLEGIO SANTO TOMAS','circuitofiscal'=>24,'numero_mesas'=>9,'faltan_mesas'=>9,'direccion'=>'PEDRO DE MENDOZA 180 (EL COLMENAR)','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA SECUNDARIA Bª S RAMON','circuitofiscal'=>25,'numero_mesas'=>10,'faltan_mesas'=>10,'direccion'=>'AVDA. SAN RAMON 1000','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA MEDIA RAMON A. ARAUJO','circuitofiscal'=>26,'numero_mesas'=>18,'faltan_mesas'=>18,'direccion'=>'JOSE I. WARNES 500','disponible'=>'si','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'COLEGIO NTRA SRA DE FATIMA','circuitofiscal'=>27,'numero_mesas'=>6,'faltan_mesas'=>6,'direccion'=>'MUÑECAS 1570','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'CLUB 13 DE MAYO','circuitofiscal'=>27,'numero_mesas'=>7,'faltan_mesas'=>7,'direccion'=>'MAIPU 1577','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESC DE MANUAL Y PRACT D HOGAR','circuitofiscal'=>28,'numero_mesas'=>8,'faltan_mesas'=>8,'direccion'=>'ESPAÑA 1550','disponible'=>'si','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA MIGUEL LILLO','circuitofiscal'=>28,'numero_mesas'=>9,'faltan_mesas'=>9,'direccion'=>'ESPAÑA 1765','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA TECNICA Nº 4 JUAN XXIII','circuitofiscal'=>28,'numero_mesas'=>8,'faltan_mesas'=>8,'direccion'=>'CHILE 2151','disponible'=>'si','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA NIÑO JESUS DE PRADA','circuitofiscal'=>28,'numero_mesas'=>8,'faltan_mesas'=>8,'direccion'=>'THAMES 1280','disponible'=>'si','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'CLUB ASOCIACION MITRE','circuitofiscal'=>28,'numero_mesas'=>6,'faltan_mesas'=>6,'direccion'=>'BELGRANO 1547','disponible'=>'si','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'COLEGIO DON ORIONE','circuitofiscal'=>29,'numero_mesas'=>16,'faltan_mesas'=>16,'direccion'=>'AVDA. REPUBLICA DEL LIBANO 2148','disponible'=>'si','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA MONS. BLAS V CONRERO','circuitofiscal'=>29,'numero_mesas'=>17,'faltan_mesas'=>17,'direccion'=>'EMILIO CASTELAR 1903','disponible'=>'si','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA ZENON SANTILLAN','circuitofiscal'=>30,'numero_mesas'=>12,'faltan_mesas'=>12,'direccion'=>'CASTELLI ESQ. PJE. UNAMUNO','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA MANUEL LAINEZ','circuitofiscal'=>30,'numero_mesas'=>11,'faltan_mesas'=>11,'direccion'=>'ISABEL LA CATOLICA ESQ CASTRO BARROS','disponible'=>'si','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESC ILDEFONSO DE LAS MUÑECAS','circuitofiscal'=>30,'numero_mesas'=>10,'faltan_mesas'=>10,'direccion'=>'FEDERICO HELGUERA 2300','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESC.SALUSTIANO ZAVALIA','circuitofiscal'=>30,'numero_mesas'=>10,'faltan_mesas'=>10,'direccion'=>'NECOCHEA ESQ. PJE MONSERRAT','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA EJERCITO ARGENTINO','circuitofiscal'=>30,'numero_mesas'=>10,'faltan_mesas'=>10,'direccion'=>'CASTRO BARROS 1700','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA ESPECIAL ELSA RUGGERI DE FABIO','circuitofiscal'=>30,'numero_mesas'=>9,'faltan_mesas'=>9,'direccion'=>'ECUADOR 3800','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA REPUBLICA DEL PARAGUAY','circuitofiscal'=>31,'numero_mesas'=>10,'faltan_mesas'=>10,'direccion'=>'CHILE 3500','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'COLEGIO NTRA SRA DE MONSERRAT','circuitofiscal'=>31,'numero_mesas'=>10,'faltan_mesas'=>10,'direccion'=>'COLOMBIA 2937','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'COLEGIO SAN JOSE DE CALASANZ','circuitofiscal'=>31,'numero_mesas'=>10,'faltan_mesas'=>10,'direccion'=>'ITALIA 3302','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA SECUNDARIA CAMPO NORTE','circuitofiscal'=>31,'numero_mesas'=>10,'faltan_mesas'=>10,'direccion'=>'BOLIVIA 2500 ESQ CASTELLI','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA NUEVA ESPERANZA','circuitofiscal'=>32,'numero_mesas'=>9,'faltan_mesas'=>9,'direccion'=>'PERU 4500','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA DE COMERCIO Nº4','circuitofiscal'=>32,'numero_mesas'=>9,'faltan_mesas'=>9,'direccion'=>'BEL. ROLDAN ESQ.PERU-BºOESTE II','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA SOBERANIA NACIONAL','circuitofiscal'=>32,'numero_mesas'=>9,'faltan_mesas'=>9,'direccion'=>'BOLIVIA 4100','disponible'=>'si','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'COLEGIO NUEVA AMERICA','circuitofiscal'=>32,'numero_mesas'=>9,'faltan_mesas'=>9,'direccion'=>'PERU 3733','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'COLEGIO NUESTRA SRA DE LUJAN','circuitofiscal'=>33,'numero_mesas'=>9,'faltan_mesas'=>9,'direccion'=>'DON BOSCO 2645','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'COLEGIO ALMAFUERTE','circuitofiscal'=>33,'numero_mesas'=>9,'faltan_mesas'=>9,'direccion'=>'AVDA. BELGRANO 3500','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA MARCOS PAZ','circuitofiscal'=>33,'numero_mesas'=>9,'faltan_mesas'=>9,'direccion'=>'MENDOZA 2680','disponible'=>'si','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA GABRIELA MISTRAL','circuitofiscal'=>33,'numero_mesas'=>9,'faltan_mesas'=>9,'direccion'=>'JUAN JOSE PASSO 167','disponible'=>'si','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA PAUL GROUSSAC','circuitofiscal'=>33,'numero_mesas'=>10,'faltan_mesas'=>10,'direccion'=>'ESPAÑA 2780','disponible'=>'si','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA A GARCIA HAMILTON','circuitofiscal'=>34,'numero_mesas'=>7,'faltan_mesas'=>7,'direccion'=>'FELIX DE OLAZABAL 350','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'COLEGIO SAN IGNACIO DE LOYOLA','circuitofiscal'=>34,'numero_mesas'=>6,'faltan_mesas'=>6,'direccion'=>'BELISARIO ROLDAN 150','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'INSTITUTO KINDER','circuitofiscal'=>34,'numero_mesas'=>9,'faltan_mesas'=>9,'direccion'=>'ESTEBAN ECHEVERRIA 256','disponible'=>'si','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA TECNICA Nº 5','circuitofiscal'=>34,'numero_mesas'=>7,'faltan_mesas'=>7,'direccion'=>'PATRICIAS ARGENTINAS 383','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'COLEGIO DEL SOL','circuitofiscal'=>34,'numero_mesas'=>5,'faltan_mesas'=>5,'direccion'=>'ESTEBAN ECHEVERRIA 256','disponible'=>'si','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA CIUDADELA','circuitofiscal'=>35,'numero_mesas'=>12,'faltan_mesas'=>12,'direccion'=>'AV COLON ESQ. LAMADRID','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA SCALABRINI ORTIZ','circuitofiscal'=>35,'numero_mesas'=>10,'faltan_mesas'=>10,'direccion'=>'PJE. BENJAMIN PAZ 231','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA PERIODISMO ARGENTINO','circuitofiscal'=>35,'numero_mesas'=>11,'faltan_mesas'=>11,'direccion'=>'PJE. BENJAMIN PAZ 251','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA MARCOS AVELLANEDA','circuitofiscal'=>35,'numero_mesas'=>5,'faltan_mesas'=>5,'direccion'=>'GRAL. LAVALLE 3461','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'INSTITUTO ADVENTISTA DE TUCUMAN','circuitofiscal'=>35,'numero_mesas'=>10,'faltan_mesas'=>10,'direccion'=>'SAN LORENZO 2910','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA CAPITAN DE LOS ANDES','circuitofiscal'=>36,'numero_mesas'=>11,'faltan_mesas'=>11,'direccion'=>'ALSINA 4771','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA MEDIA Bº PDTE. PERON','circuitofiscal'=>37,'numero_mesas'=>13,'faltan_mesas'=>13,'direccion'=>'AV.INDEPENDENCIA 4000','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA PARA LA VIDA','circuitofiscal'=>37,'numero_mesas'=>15,'faltan_mesas'=>15,'direccion'=>'MANUEL COSSIO 1100-BºPDTE PERON','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA MUTUAL POLICIAL','circuitofiscal'=>38,'numero_mesas'=>15,'faltan_mesas'=>15,'direccion'=>'AVDA. INDEPENDENCIA ESQ. GORRITI','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA 248 - PRIMARIA','circuitofiscal'=>38,'numero_mesas'=>13,'faltan_mesas'=>13,'direccion'=>'LINCOLN 1451','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA LIZONDO BORDA','circuitofiscal'=>39,'numero_mesas'=>11,'faltan_mesas'=>11,'direccion'=>'PUEYRREDON 949','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'FACULTAD DE CIENCIAS ECONOMICAS','circuitofiscal'=>39,'numero_mesas'=>6,'faltan_mesas'=>6,'direccion'=>'INDEPENDENCIA Y PELLEGRINI','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA ALFREDO COSSON','circuitofiscal'=>40,'numero_mesas'=>18,'faltan_mesas'=>18,'direccion'=>'CORONEL ZELAYA 1600','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA SECUND. - J.L.NOUGUES','circuitofiscal'=>40,'numero_mesas'=>11,'faltan_mesas'=>11,'direccion'=>'LARREA 3050','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA SECUN. - MAESTRO ARANCIBIA','circuitofiscal'=>41,'numero_mesas'=>14,'faltan_mesas'=>14,'direccion'=>'TORRES POSSE 2800','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA S.S.JUAN PABLO II','circuitofiscal'=>42,'numero_mesas'=>9,'faltan_mesas'=>9,'direccion'=>'JUAN PADROS ESQ. E.FLAVIO VIRLA','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESC SECUNDARIA  Bº SAN MIGUEL','circuitofiscal'=>42,'numero_mesas'=>8,'faltan_mesas'=>8,'direccion'=>'VICENTE GALLO 2700','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA CAMPO DE LAS CARRERAS','circuitofiscal'=>43,'numero_mesas'=>10,'faltan_mesas'=>10,'direccion'=>'AVDA. NESTOR KIRCHNER 1444','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA SILVANO BORES','circuitofiscal'=>43,'numero_mesas'=>11,'faltan_mesas'=>11,'direccion'=>'O HIGGINS 1145','disponible'=>'si','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESC. 372 - CAMPO DE LAS CARRERAS','circuitofiscal'=>43,'numero_mesas'=>12,'faltan_mesas'=>12,'direccion'=>'ALSINA 1455','disponible'=>'si','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA SECUND. B. VICTORIA','circuitofiscal'=>43,'numero_mesas'=>11,'faltan_mesas'=>11,'direccion'=>'MATHEU 1803','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA SECUNDARIA BºSUTIAGA','circuitofiscal'=>44,'numero_mesas'=>12,'faltan_mesas'=>12,'direccion'=>'AVDA. COLON ESQ. EUDORO ARAOZ','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA EUDORO ARAOZ','circuitofiscal'=>44,'numero_mesas'=>7,'faltan_mesas'=>7,'direccion'=>'EUDORO ARAOZ 1750','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA OBISPO COLOMBRES','circuitofiscal'=>44,'numero_mesas'=>6,'faltan_mesas'=>6,'direccion'=>'FRIAS SILVA 2800','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA DE TODOS','circuitofiscal'=>44,'numero_mesas'=>6,'faltan_mesas'=>6,'direccion'=>'LIBERTAD 2300','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA GUILLERMO GRIET','circuitofiscal'=>45,'numero_mesas'=>15,'faltan_mesas'=>15,'direccion'=>'MAGALLANES 950','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA DEAN FUNES','circuitofiscal'=>45,'numero_mesas'=>10,'faltan_mesas'=>10,'direccion'=>'PJE. OLLEROS 1200','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA DEL MERCOFRUT','circuitofiscal'=>45,'numero_mesas'=>11,'faltan_mesas'=>11,'direccion'=>'EXEQUIEL COLOMBRES 400','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA SECUNDARIA EL SALVADOR','circuitofiscal'=>45,'numero_mesas'=>13,'faltan_mesas'=>13,'direccion'=>'AVDA. A.VESPUCIO Y AV CIRCUNVALACION','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA NUEVA VILLA AMALIA','circuitofiscal'=>45,'numero_mesas'=>10,'faltan_mesas'=>10,'direccion'=>'MORENO 2100','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA TECNICA ROGELIO GUZMAN','circuitofiscal'=>45,'numero_mesas'=>6,'faltan_mesas'=>6,'direccion'=>'JUJUY 2100','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA Bº SAN ALBERTO','circuitofiscal'=>45,'numero_mesas'=>6,'faltan_mesas'=>6,'direccion'=>'CANGALLO 1400','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA MIGUEL CRITTO','circuitofiscal'=>46,'numero_mesas'=>12,'faltan_mesas'=>12,'direccion'=>'9 DE JULIO 3100','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA MARTIN M DE GUEMES(PR)','circuitofiscal'=>46,'numero_mesas'=>12,'faltan_mesas'=>12,'direccion'=>'PJE. PISARELLO ESQ. JUJUY','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA MARTIN M DE GUEMES(SE)','circuitofiscal'=>46,'numero_mesas'=>10,'faltan_mesas'=>10,'direccion'=>'DR. RENE FAVALORO ESQ. AYACUCHO','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA SECUN.CRUCERO BELGRANO','circuitofiscal'=>46,'numero_mesas'=>9,'faltan_mesas'=>9,'direccion'=>'MERCEDES SAN MARTIN ESQ. IRAMAIN','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA AMADO NICOMEDES JURI','circuitofiscal'=>46,'numero_mesas'=>7,'faltan_mesas'=>7,'direccion'=>'MARINA ALFARO 3100','disponible'=>'si','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA Nº 253 ESTADO DE ISRAEL','circuitofiscal'=>46,'numero_mesas'=>9,'faltan_mesas'=>9,'direccion'=>'AV ALEM 3800','disponible'=>'si','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA PANTALEON FERNANDEZ','circuitofiscal'=>47,'numero_mesas'=>14,'faltan_mesas'=>14,'direccion'=>'BUENOS AIRES 4500','disponible'=>'no','estado'=>'incompleto']);
        Escuela::create(['nombre_escuela'=>'ESCUELA ESPECIAL MAGDALENA LAGARRIGUE','circuitofiscal'=>47,'numero_mesas'=>7,'faltan_mesas'=>7,'direccion'=>'BUENOS AIRES 3900','disponible'=>'no','estado'=>'incompleto']);

        }
}

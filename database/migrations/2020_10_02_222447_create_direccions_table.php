<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDireccionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('direcciones', function (Blueprint $table) {
            $table->id('iddireccion');
            $table->string('calle');
            $table->integer('numero');
            $table->string('piso')->nullable();
            $table->string('dpto')->nullable();
            $table->string('manzana')->nullable();
            $table->string('casa')->nullable();
            $table->unsignedBigInteger('localidad_idlocalidad');
            $table->timestamps();
            
            $table->foreign('localidad_idlocalidad')
                        ->references('idlocalidad')
                        ->on('localidades')
                        ->onUpdate('cascade')
                        ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('direcciones');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMesasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mesas', function (Blueprint $table) {
            $table->id('idmesa');
            $table->bigInteger('numero_mesa');
            $table->string('disponible');
            $table->string('estado');
            $table->unsignedBigInteger('id_escuela');
            $table->timestamps();

            $table->foreign('id_escuela')
            ->references('idescuela')
            ->on('escuelas')
            ->onUpdate('cascade')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mesas');
    }
}

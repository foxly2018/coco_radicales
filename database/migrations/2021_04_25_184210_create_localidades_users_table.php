<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLocalidadesUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('localidades_users', function (Blueprint $table) {
            $table->id();            
            $table->bigInteger('localidades_idlocalidad')->unsigned();
            $table->bigInteger('users_id')->unsigned();
            $table->bigInteger('es_responsable')->nullable();
            $table->timestamps();

            $table->foreign('localidades_idlocalidad')
                  ->references('idlocalidad')
                  ->on('localidades')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');

            $table->foreign('users_id')
                  ->references('id')
                  ->on('users')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('localidades_users');
    }
}

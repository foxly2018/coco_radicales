<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->string('apellido');
            $table->bigInteger('dni');
            $table->bigInteger('celular')->nullable();
            $table->bigInteger('telefono')->nullable();
            $table->date('fecha_nacimiento');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            //$table->unsignedBigInteger('user_iddireccion')->nullable();
            $table->unsignedInteger('estado');//entero sin signo hasta 10
            $table->rememberToken();
            $table->timestamps();

            /* $table->foreign('user_iddireccion')
                        ->references('iddireccion')
                        ->on('direcciones')
                        ->onUpdate('cascade')
                        ->onDelete('cascade'); */
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

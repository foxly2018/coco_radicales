<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEscuelasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('escuelas', function (Blueprint $table) {
            $table->id('idescuela');
            $table->string('nombre_escuela');
            $table->unsignedBigInteger('circuitofiscal');
            $table->bigInteger('numero_mesas');
            $table->bigInteger('faltan_mesas')->nullable();
            $table->string('direccion');
            $table->string('disponible'); //si o no
            $table->string('estado'); //completo o incompleto
            $table->string('comida')->nullable();
            $table->unsignedBigInteger('fiscal_general')->nullable(); //fiscal general

            $table->timestamps();

            $table->foreign('circuitofiscal')
            ->references('idcircuitofiscal')
            ->on('circuito_fiscales')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->foreign('fiscal_general')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('escuelas');
    }
}

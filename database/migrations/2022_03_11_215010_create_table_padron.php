<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTablePadron extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('padron', function (Blueprint $table) {
            $table->id('idpadron');
            $table->bigInteger('dni');
            $table->string('tipo_ejemplar');
            $table->string('apellido');
            $table->string('Nombre');
            $table->string('domicilio');
            $table->string('Circuito2');
            $table->string('Localidad');
            $table->string('Departamento');
            $table->string('Seccion');
            $table->string('sexo');
            $table->bigInteger('mesa');
            $table->bigInteger('orden_mesa');
            $table->string('establecim');
            $table->string('domic_establec');
            $table->string('local_establec');
            $table->string('depart_establec');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('padron', function (Blueprint $table) {
            Schema::dropIfExists('padron');
        });
    }
}

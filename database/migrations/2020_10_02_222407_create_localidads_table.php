<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLocalidadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('localidades', function (Blueprint $table) {
            $table->id('idlocalidad');
            $table->string('localidad_nombre');
            $table->unsignedBigInteger('departamento_iddepartamento');
            $table->timestamps();
            
            $table->foreign('departamento_iddepartamento')
                        ->references('iddepartamento')
                        ->on('departamentos')
                        ->onUpdate('cascade')
                        ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('localidades');
    }
}

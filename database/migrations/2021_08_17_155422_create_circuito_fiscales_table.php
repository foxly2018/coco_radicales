<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCircuitoFiscalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('circuito_fiscales', function (Blueprint $table) {
            $table->id('idcircuitofiscal');
            $table->string('circuito_nombre');
            $table->unsignedBigInteger('localidad_idlocalidad');
            $table->bigInteger('cantidadEscuelas');
            $table->bigInteger('cantidadMesas');
            $table->string('disponible'); //si o no
            $table->string('estado'); //completo o incompleto
            $table->timestamps();

            $table->foreign('localidad_idlocalidad')
                        ->references('idlocalidad')
                        ->on('localidades')
                        ->onUpdate('cascade')
                        ->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('circuito_fiscales');
    }
}

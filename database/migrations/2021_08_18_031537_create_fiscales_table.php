<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFiscalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fiscales', function (Blueprint $table) {
            $table->id('idfiscales');
            $table->unsignedBigInteger('id_voluntario');
            $table->bigInteger('circuito_fiscal')->nullable();
            $table->unsignedBigInteger('id_mesa');
            $table->unsignedBigInteger('id_escuela')->nullable();
            $table->string('tipo');
            $table->string('asistencia')->nullable();
            $table->string('capacitacion')->nullable();
            $table->timestamps();


            $table->foreign('id_voluntario')
            ->references('idvoluntario')
            ->on('voluntarios')
            ->onDelete('cascade');

            $table->foreign('id_mesa')
            ->references('idmesa')
            ->on('mesas')
            ->onDelete('cascade');

            $table->foreign('id_escuela')
            ->references('idescuela')
            ->on('escuelas')
            ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fiscales');
    }
}

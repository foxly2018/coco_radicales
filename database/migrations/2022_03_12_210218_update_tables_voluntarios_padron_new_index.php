<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTablesVoluntariosPadronNewIndex extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('voluntarios', function (Blueprint $table) {
            $table->index('dni');
        });
        Schema::table('padron', function (Blueprint $table) {
            $table->index('dni');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('voluntarios', function (Blueprint $table) {
            $table->dropIndex('voluntarios_dni_index');
        });
        Schema::table('padron', function (Blueprint $table) {
            $table->dropIndex('padron_dni_index');
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInteraccionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interacciones', function (Blueprint $table) {
            $table->id();
            $table->string('descripcion')->nullable();
            $table->date('fecha')->nullable();
            $table->unsignedInteger('estado');
            $table->bigInteger('voluntarios_idvoluntario')->unsigned();
            $table->bigInteger('users_id')->unsigned();
            $table->bigInteger('tipo_interaccion_id')->unsigned();
            $table->timestamps();

            $table->foreign('voluntarios_idvoluntario')
                  ->references('idvoluntario')
                  ->on('voluntarios')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');

            $table->foreign('users_id')
                  ->references('id')
                  ->on('users')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');

            $table->foreign('tipo_interaccion_id')
                  ->references('id')
                  ->on('tipo_interaccion')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interacciones');
    }
}

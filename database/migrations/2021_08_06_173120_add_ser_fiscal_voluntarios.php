<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSerFiscalVoluntarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('voluntarios', function (Blueprint $table) {
            $table->bigInteger('serFiscal')->nullable()->after('cantNegativas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('voluntarios', function (Blueprint $table) {
            $table->dropColumn('serFiscal');
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCampoValidacioninteraccionEnVoluntatiosEInteracciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('voluntarios', function (Blueprint $table) {
            //Hubo interaccion = 1
            //No hubo interaccion = 2
            $table->bigInteger('confirmarInteraccion')->nullable()->after('origen');
            //positiva = 1
            //Negativa = 2
            $table->bigInteger('calidadInteraccion')->nullable()->after('confirmarInteraccion');
            $table->bigInteger('cantNegativas')->nullable()->after('calidadInteraccion');
        });
        Schema::table('interacciones', function (Blueprint $table) {
            //positiva = 1
            //Negativa = 2
            $table->bigInteger('calidad')->nullable()->after('tipo_interaccion_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('voluntarios', function (Blueprint $table) {
            $table->dropColumn(['confirmarInteraccion','calidadInteraccion','cantNegativas']);
        });
        Schema::table('interacciones', function (Blueprint $table) {
            $table->dropColumn(['calidad']);
        });
    }
}
